const path = require("path");
const withImages = require("next-images");
require("dotenv").config();

module.exports = withImages({
  distDir: "build",
  env: {
    API_URL: process.env.API_URL,
  },
  publicRuntimeConfig: {
    API_URL: process.env.API_URL,
  },
  exclude: path.resolve(__dirname, "public/images/icons/svg"),
  webpack: (config) => {
    // config.resolve.alias["~"] = path.resolve(__dirname);
    config.resolve.alias["components"] = path.join(__dirname, "components");
    // config.resolve.alias["public"] = path.join(__dirname, "public");
    config.resolve.alias["styles"] = path.join(__dirname, "styles");
    // config.resolve.alias["utils"] = path.join(__dirname, "styles/utils");
    config.resolve.alias["images"] = path.join(__dirname, "public/images");
    // config.resolve.alias["fonts"] = path.join(__dirname, "/static/fonts");

    return config;
  },
});
