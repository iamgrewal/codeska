import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import PageDescription from 'components/PageDescription';
import Banner from 'components/Banner';
import { NextSeo } from 'next-seo';
const { API_URL } = process.env;

const Csr = ({ data }) => {
  const { header, description, banner_block } = data;
  const SEO = {
    title: 'Csr Codeska - IT Production Company',
    description: 'Csr Codeska - IT Production Company',
  };
  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          imgSrc={header.image.url}
          hidden={header.hidden}
        />
      )}
      <main>
        {description && (
          <PageDescription
            title={description.title}
            text={description.text}
            hidden={description.hidden}
            small={true}
          />
        )}

        <article className='news-post'>
          <div className='container'>
            <div className='row news-post__row mb-20'>
              <div className='col-sm-12 col-lg-12 news-post__image-wrapper image-wrapper'>
                <img
                  className='img-responsive img-responsive--cover'
                  src={
                    data.main_image ? `${API_URL}${data.main_image.url}` : null
                  }
                  alt='article preview'
                />
              </div>
            </div>
            <div className='row  align-items-end section-margin-content'>
              <div className='col-sm-12 col-md-12 col-lg-8 news-post__col'>
                <div className='news-post__text-group'>
                  {data.main_image_paragraph
                    ? data.main_image_paragraph.map((item) => (
                        <p className='news-post__text' key={item.id}>
                          {item.text}
                        </p>
                      ))
                    : null}
                </div>
                <div className='news-post__text-group'>
                  <h2 className='news-post__caption caption caption--h2 caption--700'>
                    {data.main_image_title ? data.main_image_title : null}
                  </h2>
                  {data.paragraph_second
                    ? data.paragraph_second.map((item) => (
                        <p className='news-post__paragraph' key={item.id}>
                          {item.text}
                        </p>
                      ))
                    : null}
                </div>
              </div>
              <aside className='col-lg-4 news-post__sidebar sidebar-scr'>
                <div className='sidebar__item sidebar__item--image'>
                  <div className='sidebar__image-wrapper image-wrapper'>
                    <img
                      className='img-responsive'
                      src={
                        data.sidebar_image
                          ? `${API_URL}${data.sidebar_image.url}`
                          : ''
                      }
                      alt='sidebar-image'
                    />
                  </div>
                  <span className='sidebar__image-description'>
                    {data.sidebar_image_descript
                      ? data.sidebar_image_descript
                      : null}
                  </span>
                </div>
              </aside>
            </div>
          </div>
          <Banner key={banner_block.id} data={banner_block} />
          <div className='container'>
            <div className='row  align-items-end section-margin-content'>
              <div className='col-sm-12 col-md-12 col-lg-8 news-post__col'>
                <div className='news-post__text-group'>
                  <div className='news-post__image'>
                    <img
                      className='img-responsive img-responsive--cover'
                      src={data.image ? `${API_URL}${data.image.url}` : null}
                      alt='article preview'
                    />
                  </div>
                  <h3 className='news-post__caption caption caption--h3 caption--800'>
                    {data.image_title ? data.image_title : null}
                  </h3>
                  {data.paragraph
                    ? data.paragraph.map((item) => (
                        <p className='news-post__paragraph' key={item.id}>
                          {item.text}
                        </p>
                      ))
                    : null}
                </div>
              </div>
              <aside className='col-lg-4 news-post__sidebar sidebar-scr'>
                <div className='sidebar__item text-pannel'>
                  {data.sidebar_text ? data.sidebar_text : null}
                </div>
              </aside>
            </div>
          </div>
        </article>
      </main>
    </>
  );
};

export async function getServerSideProps() {
  const res = await fetch(`${API_URL}/csr-page`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default Csr;
