import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import PageDescription from 'components/PageDescription';
import Partners from 'components/Partners';
import Ceo from 'components/Ceo';
import Banner from 'components/Banner';
import OurServices from 'components/OurServices';
import Slider from 'components/Slider';
import CasePreview from 'components/CasePreview';
const { API_URL } = process.env;
import { NextSeo } from 'next-seo';

const Home = ({ data }) => {
  const {
    header,
    description,
    partners_list,
    services,
    ceo_block,
    banner_block,
    news_slider,
    case_preview,
  } = data;

  const SEO = {
    title: 'Codeska - IT Production Company',
    description: 'Codeska - IT Production Company',
  };
  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          ppt={header.ppt}
          imgSrc={header.image.url}
          main={true}
          hidden={header.hidden}
        />
      )}
      <main>
        {description && (
          <PageDescription
            title={description.title}
            text={description.text}
            hidden={description.hidden}
          />
        )}
        {services && <OurServices services={services} />}
        {banner_block
          ? banner_block.map((item) =>
              item.type === 'truck' ? (
                <Banner key={item.id} data={item} />
              ) : null
            )
          : null}
        {ceo_block && <Ceo ceo={ceo_block} company={true} />}
        {partners_list && <Partners partners={partners_list} />}
        {banner_block
          ? banner_block.map((item) =>
              item.type === 'ship' ? <Banner key={item.id} data={item} /> : null
            )
          : null}
        {case_preview && <CasePreview data={case_preview} />}
        {news_slider && <Slider data={news_slider} news={true} />}
      </main>
    </>
  );
};

export async function getServerSideProps() {
  const res = await fetch(`${API_URL}/company`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default Home;
