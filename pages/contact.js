import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import Banner from 'components/Banner';
import OurContacts from 'components/OurContacts';
import EmbedFeed from 'components/EmbedFeed';
import { NextSeo } from 'next-seo';
const { API_URL } = process.env;

function Contact({ data }) {
  const { header, embed_feed, contacts, banner_block } = data;
  const SEO = {
    title: 'Contact Codeska - IT Production Company',
    description: 'Contact Codeska - IT Production Company',
  };
  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          ppt={header.ppt}
          imgSrc={header.image.url}
          main={true}
          hidden={header.hidden}
        />
      )}
      <main>
        {contacts && <OurContacts data={contacts} />}
        {banner_block ? (
          <Banner key={banner_block.id} data={banner_block} />
        ) : null}
        {embed_feed && <EmbedFeed data={embed_feed} />}
      </main>
    </>
  );
}

export async function getServerSideProps() {
  const res = await fetch(`${API_URL}/contact`);
  const data = await res.json();
  return {
    props: {
      data,
    },
  };
}

export default Contact;
