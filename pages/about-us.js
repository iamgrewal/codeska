import fetch from 'isomorphic-unfetch';
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import Markdown from 'components/Markdown';
import Header from 'components/Header';
import PageDescription from 'components/PageDescription';
import Banner from 'components/Banner';
import Ceo from 'components/Ceo';
import Csr from 'components/Csr';
import OurServices from 'components/OurServices';
import Slider from 'components/Slider';
import Partners from 'components/Partners';
import CasePreview from 'components/CasePreview';

const { API_URL } = process.env;

function About({ data }) {
  const {
    header,
    description,
    banner_block,
    ceo_block,
    services,
    about_career,
    news_slide,
    partners_list,
    csr_block,
    case_preview,
  } = data;

  const SEO = {
    title: 'About Codeska - IT Production Company',
    description: 'About Codeska - IT Production Company',
  };
  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          imgSrc={header.image.url}
          hidden={header.hidden}
        />
      )}
      <main>
        {description && (
          <PageDescription
            title={description.title}
            text={description.text}
            hidden={description.hidden}
          />
        )}
        {ceo_block && <Ceo ceo={ceo_block} company={true} />}
        {services && <OurServices services={services} />}
        {banner_block ? (
          <Banner key={banner_block.id} data={banner_block} />
        ) : null}
        {partners_list && <Partners partners={partners_list} />}
        {csr_block && <Csr csr={csr_block} company={true} />}
        {about_career && !about_career.hidden ? (
          <section className='careers section-margin'>
            <article className='careers-prev'>
              <img
                className='img-responsive img-responsive--cover careers-prev__image'
                src={
                  about_career.image.url &&
                  `${API_URL}${about_career.image.url}`
                }
                alt='image'
              />
              <div className='careers-prev__container container'>
                <Markdown
                  className='careers-prev__caption caption caption--h2 caption--800'
                  source={about_career.title && about_career.title}
                />
                <Markdown
                  className='careers-prev__description'
                  source={about_career.text && about_career.text}
                />
                <Link href='/careers'>
                  <a>
                    <button type='button' className='careers-prev__btn btn'>
                      <span>
                        {about_career.button_text && about_career.button_text}
                      </span>
                    </button>
                  </a>
                </Link>
              </div>
            </article>
          </section>
        ) : null}
        {case_preview && <CasePreview data={case_preview} />}
        {news_slide && <Slider data={news_slide} news={true} />}
      </main>
    </>
  );
}

export async function getServerSideProps() {
  const res = await fetch(`${API_URL}/about-us`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default About;
