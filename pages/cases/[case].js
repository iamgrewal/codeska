import Link from 'next/link';
import { NextSeo } from 'next-seo';
import Glide from '@glidejs/glide';
import { useEffect, useRef } from 'react';
import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import Markdown from 'components/Markdown';
import CaseAdvantage from 'components/CaseAdvantage';
import parse from 'html-react-parser';

const { API_URL } = process.env;

const Case = ({ data }) => {
  console.log(data);
  const {
    header,
    case_description,
    project_status,
    business,
    difference,
    case_banner,
    tech_list,
    technologies_items,
    advantages_title,
    advantages_item,
    user_interface,
    user_design,
    user_review,
  } = data;
  const SEO = {
    title: `Case Codeska`,
    description: `Case Codeska`,
  };

  const ref = useRef();

  useEffect(() => {
    if (ref?.current) {
      const slider = new Glide(ref.current, {
        type: `carousel`,
        startAt: 0,
        gap: 30,
        animationDuration: 600,
        focusAt: `center`,
        slidesToShow: 1,
      });
      slider.mount();
    }
  }, [ref]);

  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          ppt={header.ppt}
          imgSrc={header.image.url}
          currentCase={true}
          hidden={header?.hidden}
        />
      )}
      <main>
        <section className='page-info section-margin-content'>
          <div className='container'>
            {case_description?.logo?.url && (
              <img
                className='blog-post-prev__logo mb-20'
                src={`${API_URL}${case_description.logo.url}`}
                alt='logo'
              />
            )}
            <div className='row'>
              {case_description?.description &&
                case_description?.description.map((item) => (
                  <Markdown
                    key={item?.id}
                    className='page-description__text col-sm-12 col-md-6'
                    source={item?.text}
                  />
                ))}
            </div>
            {case_description?.case_link && (
              <a className='hero-link' href={case_description.case_link}>
                {case_description.case_link}
              </a>
            )}
          </div>
        </section>

        <article className='case'>
          {!project_status?.hidden && (
            <div className='container'>
              <div className='row'>
                <section className='col-12 case__status section-margin'>
                  {project_status?.title ? (
                    <h2 className='case__caption caption caption--h2 caption--800'>
                      {project_status?.title}
                    </h2>
                  ) : null}
                  {project_status?.iteration_title ? (
                    <h4 className='case__subcaption iteration-title'>
                      {project_status?.iteration_title}
                    </h4>
                  ) : null}

                  <div className='case__iterarion'>
                    <Markdown className='' source={project_status?.iteration} />
                  </div>
                  <div className='row '>
                    {project_status?.features.length !== 0
                      ? project_status?.features.map((item) => {
                          if (!item?.hidden) {
                            return (
                              <div
                                className='col-12 col-lg-3 col-xl-3'
                                key={item?.id}
                              >
                                <div className=' status__item'>
                                  <h4 className='case__subcaption'>
                                    {item?.title}
                                  </h4>
                                  <p>{item?.text}</p>
                                </div>
                              </div>
                            );
                          } else {
                            return null;
                          }
                        })
                      : null}
                  </div>
                </section>
              </div>
            </div>
          )}

          {!business?.hidden &&
          business?.image &&
          business?.image.length !== 0 ? (
            <div className='container'>
              <div className='row'>
                <section className='case__business section-margin'>
                  <div className='col-12'>
                    <div className='row '>
                      <div className='col-12 col-lg-6 col-xl-6'>
                        <h2 className='case__caption caption caption--h2 caption--800'>
                          {business?.title}
                        </h2>
                        <div className='col-12 col-lg-6 col-xl-6 d-sm-block d-lg-none mb-30 p-0'>
                          {business?.image.length !== 0
                            ? business?.image.map((item) => (
                                <div className='mb-10' key={item?.id}>
                                  <img
                                    className='img-responsive'
                                    src={
                                      item?.url ? `${API_URL}${item.url}` : ''
                                    }
                                    alt='sidebar-image'
                                  />
                                </div>
                              ))
                            : null}
                        </div>
                        {business?.content.length !== 0
                          ? business?.content.map((item) => {
                              if (!item?.hidden) {
                                return (
                                  <div
                                    className='case__business-content'
                                    key={item.id}
                                  >
                                    <h4 className='case__subcaption-sm'>
                                      {item?.title}
                                    </h4>
                                    <p>
                                      {parse(
                                        item?.text.replace(
                                          /(?:\r\n|\r|\n)/g,
                                          '<br>'
                                        )
                                      )}
                                      {/* {item?.text} */}
                                    </p>
                                  </div>
                                );
                              } else {
                                return null;
                              }
                            })
                          : null}
                      </div>
                      <div className='col-12 col-lg-6 col-xl-6 d-sm-none d-lg-block'>
                        {business?.image.length !== 0
                          ? business?.image.map((item) => (
                              <div className='mb-10' key={item?.id}>
                                <img
                                  className='img-responsive'
                                  src={item?.url ? `${API_URL}${item.url}` : ''}
                                  alt='sidebar-image'
                                />
                              </div>
                            ))
                          : null}
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          ) : null}

          {!difference?.hidden &&
          difference &&
          difference?.items.length !== 0 ? (
            <div className='container'>
              <div className='row'>
                <section className=' case__difference section-margin'>
                  <div className='col-12'>
                    <h2 className='case__caption caption caption--h2 caption--800'>
                      {difference?.title}
                    </h2>
                    <div className='row'>
                      {difference?.items.length !== 0
                        ? difference?.items.map((item) => {
                            if (!item?.hidden) {
                              return (
                                <div
                                  className='col-12 col-lg-3 col-xl-3'
                                  key={item?.id}
                                >
                                  <div className='difference__item'>
                                    <h4 className='case__subcaption'>
                                      {item?.title}
                                    </h4>
                                    <p>{item?.text}</p>
                                  </div>
                                </div>
                              );
                            } else {
                              return null;
                            }
                          })
                        : null}
                    </div>
                  </div>
                </section>
              </div>
            </div>
          ) : null}

          {!case_banner?.hidden && case_banner?.background?.url && (
            <section
              className='case__banner section-margin'
              style={
                case_banner?.background?.url && {
                  background: `url(${API_URL}${case_banner.background.url})`,
                }
              }
            >
              <div className='container case-banner__container'>
                <div className='row case-banner'>
                  <div className='case-banner__col col-12'>
                    {case_banner?.logo?.url && (
                      <img
                        className=' mb-30'
                        src={`${API_URL}${case_banner.logo.url}`}
                        alt='logo'
                      />
                    )}

                    <h3 className='case-banner__name mb-20'>
                      {case_banner?.name}
                    </h3>
                    <p className='case-banner__text'>{case_banner?.subtext}</p>
                  </div>
                </div>
              </div>
            </section>
          )}
          {technologies_items && technologies_items?.length !== 0 ? (
            <div className='container'>
              <section className='case__technologies section-margin'>
                <h2 className='case__caption caption caption--h2 caption--800'>
                  {tech_list?.title}
                </h2>
                <div className='row technologies-wrapper'>
                  {technologies_items?.length !== 0
                    ? technologies_items.map((item, index) => {
                        if (!item?.hidden) {
                          return (
                            <div key={index}>
                              <h3 className='technologies-items__name'>
                                {item.tech_item?.name}
                              </h3>
                              <div className='technologies-items' key={item.id}>
                                {item?.tech_item?.tech_info.length !== 0 &&
                                item.tech_item.tech_info.length >= 2
                                  ? item.tech_item.tech_info.map((item) => {
                                      return (
                                        <div
                                          className='technologies-items__multiple-item'
                                          key={item.id}
                                        >
                                          {item?.icon?.url && (
                                            <img
                                              className=' mb-20'
                                              src={`${API_URL}${item.icon.url}`}
                                              alt='sidebar-image'
                                            />
                                          )}
                                          <p>{item?.subtext}</p>
                                        </div>
                                      );
                                    })
                                  : item.tech_item.tech_info.map((item) => {
                                      return (
                                        <div
                                          className='technologies-items__single-item'
                                          key={item.id}
                                        >
                                          {item?.icon?.url && (
                                            <img
                                              className=' mb-20'
                                              src={`${API_URL}${item.icon.url}`}
                                              alt='sidebar-image'
                                            />
                                          )}
                                          <p>{item?.subtext}</p>
                                        </div>
                                      );
                                    })}
                              </div>
                            </div>
                          );
                        } else {
                          return null;
                        }
                      })
                    : null}
                </div>
              </section>
            </div>
          ) : null}

          {advantages_item && Object.keys(advantages_item).length ? (
            <div className='container'>
              <CaseAdvantage
                advantages_title={advantages_title}
                advantages_item={advantages_item}
              />
            </div>
          ) : null}

          {!user_interface?.hidden &&
          user_interface &&
          Object.keys(user_interface?.image).length ? (
            <section className=' case__user-interface section-margin'>
              <div className='container'>
                <h2 className='case__caption caption caption--h2 caption--800'>
                  {user_interface?.title}
                </h2>

                <div
                  className='glide glide--ltr glide--slider glide--swipeable'
                  ref={ref}
                >
                  <div
                    className='supply-schema__track glide__track'
                    data-glide-el='track'
                  >
                    <div className='glide__slides'>
                      {user_interface?.image.length !== 0
                        ? user_interface?.image?.map((item, index) => (
                            <div className='glide__slide' key={index}>
                              <img
                                className='case__slider-img'
                                src={item?.url ? `${API_URL}${item.url}` : ''}
                                alt='sidebar-image'
                              />
                            </div>
                          ))
                        : null}
                    </div>
                  </div>
                  <div className='container'>
                    <div className='case__slider-text'>
                      <p>{user_interface?.text} </p>
                    </div>
                  </div>
                  <div className='case__bullets-wrapper'>
                    <div
                      className=' glide__bullets'
                      data-glide-el='controls[nav]'
                    >
                      {user_interface?.image.length !== 0
                        ? user_interface?.image.map((item, index) => (
                            <button
                              className='case__slider-bullet glide__bullet'
                              data-glide-dir={`=${index}`}
                              key={index}
                            ></button>
                          ))
                        : null}
                    </div>
                  </div>
                </div>
              </div>
            </section>
          ) : null}

          {!user_design?.hidden &&
          user_design?.image &&
          user_design?.image.length !== 0 ? (
            <div className='container'>
              <section className='row case__user-design section-margin'>
                <div className='col-12'>
                  <div className='row justify-content-between'>
                    {!user_design?.content?.hidden && (
                      <div className='col-sm-12 col-lg-4 col-xl-4'>
                        <h2 className='case__caption caption caption--h2 caption--800'>
                          {user_design?.content?.title}
                        </h2>
                        <p>{user_design?.content?.text}</p>
                      </div>
                    )}
                    <div className='col-12 col-lg-8 col-xl-8'>
                      <div className='user-design__image-wrapper'>
                        {user_design?.image.length !== 0
                          ? user_design?.image.map((item, index) => (
                              <img
                                key={index}
                                className='user-design__image '
                                src={item?.url ? `${API_URL}${item.url}` : ''}
                                alt='image'
                              />
                            ))
                          : null}
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          ) : null}

          {!user_review?.hidden &&
          user_review?.review_item &&
          user_review?.review_item.length !== 0 ? (
            <div className='container'>
              <section className='case__user-review section-margin'>
                <h2 className='case__caption caption caption--h2 caption--800'>
                  {user_review?.title}
                </h2>
                <div className='row'>
                  {user_review?.review_item.length !== 0
                    ? user_review?.review_item.map((item, index) => (
                        <div
                          className='col-12 col-lg-4 col-xl-4 user-review__all-wrapper'
                          key={index}
                        >
                          <div className='user-review__wrapper mb-20'>
                            {item?.icon?.url && (
                              <img
                                className='user-review__quote'
                                src={`${API_URL}${item.icon.url}`}
                                alt='image'
                              />
                            )}
                            <div>
                              <h4 className='case__subcaption  mb-0'>
                                {item?.name}
                              </h4>
                              <p>{item?.person}</p>
                            </div>
                          </div>
                          <p>{item?.text}</p>
                        </div>
                      ))
                    : null}
                </div>
              </section>
            </div>
          ) : null}
        </article>
        <div className='container mb-120'>
          <Link href='/cases'>
            <a className='show-more'>
              <button className='show-more__button'>
                <span>back to cases</span>
              </button>
            </a>
          </Link>
        </div>
      </main>
    </>
  );
};

export async function getServerSideProps(context) {
  const res = await fetch(`${API_URL}/case-articles/${context.query.id}`);

  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default Case;
