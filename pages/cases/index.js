import Link from 'next/link';
import { NextSeo } from 'next-seo';

import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import PageDescription from 'components/PageDescription';
import Partners from 'components/Partners';
import Banner from 'components/Banner';
import Slider from 'components/Slider';
import CasePreview from 'components/CasePreview';

const { API_URL } = process.env;

function Cases({ data }) {
  const {
    header,
    description,
    partners_list,
    news_slider,
    case_preview,
    banner_block,
  } = data;
  const SEO = {
    title: 'Cases Codeska - IT Production Company',
    description: 'Cases Codeska - IT Production Company',
  };

  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          ppt={header.ppt}
          main={true}
          imgSrc={header.image.url}
          hidden={header.hidden}
        />
      )}

      <main>
        {description && description?.title !== '' ? (
          <PageDescription
            title={description.title}
            text={description.text}
            hidden={description.hidden}
          />
        ) : null}

        <section className='blog-posts'>
          <div className='container'>
            <div className='row blog-posts__row'>
              {case_preview && (
                <CasePreview data={case_preview} allcases={true} />
              )}
            </div>
          </div>
        </section>
        {banner_block ? (
          <Banner key={banner_block.id} data={banner_block} />
        ) : null}
        {partners_list && <Partners partners={partners_list} />}
        <Slider data={news_slider} news={true} />
      </main>
    </>
  );
}

export async function getServerSideProps() {
  const res = await fetch(`${API_URL}/cases`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default Cases;
