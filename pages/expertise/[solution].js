import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import PageDescription from 'components/PageDescription';
import Banner from 'components/Banner';
import CasePreview from 'components/CasePreview';
const { API_URL } = process.env;

const ExprtiseSolution = ({ data }) => {
  const SEO = {
    title: `Expertise Codeska`,
    description: `Expertise Codeska`,
  };

  return (
    <>
      <NextSeo {...SEO} />
      {data.header && (
        <Header
          title={data.header.title}
          text={data.header.text}
          imgSrc={data.header.image.url}
          hidden={data.header.hidden}
        />
      )}
      <main>
        <article className='news-post'>
          {data.description && (
            <PageDescription
              title={data.description.title}
              text={data.description.text}
              hidden={data.description.hidden}
            />
          )}
          <div className='container'>
            <div className='row news-post__row'>
              <div className='col news-post__wrapper'>
                <h1 className='news-post__caption caption caption--h2 caption--800 mb-60'>
                  {data.solution_title && data.solution_title}
                </h1>
                <div className='d-flex flex-wrap flex-sm-column flex-md-row'>
                  {data.main_points.length !== 0
                    ? data.main_points.map((el) => {
                        if (!el.hidden) {
                          return (
                            <div className='col-12 col-md-6 '>
                              <ul className='stage__list list'>
                                <span className='stage__group-title stage__group-title--supply'>
                                  {el.title}
                                </span>
                                <div className='col-9'>
                                  {el.item.map((point, index) => (
                                    <li className='list__item' key={index}>
                                      {point.item}
                                    </li>
                                  ))}
                                </div>
                              </ul>
                            </div>
                          );
                        } else {
                          return null;
                        }
                      })
                    : null}
                </div>
              </div>
            </div>
          </div>
          {data.banner && <Banner data={data.banner} />}
          <div className='container'>
            <div className='row news-post__row align-items-end'>
              <div className='col-sm-12 col-md-12 col-lg-8 news-post__col'>
                <div className='news-post__text-group'>
                  <div className='news-post__image'>
                    <img
                      className='img-responsive img-responsive--cover'
                      src={data.image && `${API_URL}${data.image.url}`}
                      alt='article preview'
                    />
                  </div>
                  <h3 className='news-post__caption caption caption--h3 caption--800'>
                    {data.image_title && data.image_title}
                  </h3>
                  {data.image_text.length !== 0 &&
                    data.image_text.map((item) => (
                      <p className='news-post__paragraph'>
                        {item.text && item.text}
                      </p>
                    ))}
                </div>
              </div>
              <aside className='col-lg-4 news-post__sidebar sidebar-service-sm'>
                <div className='sidebar__item text-pannel'>
                  {data.sidebar_text && data.sidebar_text}
                </div>
              </aside>
            </div>
          </div>
        </article>
        <div className='container mb-120'>
          <Link href='/expertise'>
            <a className='show-more'>
              <button className='show-more__button'>
                <span>go back</span>
              </button>
            </a>
          </Link>
        </div>
        <div className='section-margin-content'>
          {data.case_previews && <CasePreview data={data.case_previews} />}
        </div>
      </main>
    </>
  );
};

export async function getServerSideProps(context) {
  const res = await fetch(`${API_URL}/expertise-articles/${context.query.id}`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default ExprtiseSolution;
