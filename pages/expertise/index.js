import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import Banner from 'components/Banner';
import Ceo from 'components/Ceo';
import ExpertiseSlider from 'components/ExpertiseSlider';
import Slider from 'components/Slider';
import CasePreview from 'components/CasePreview';
const { API_URL } = process.env;
import { NextSeo } from 'next-seo';

function Expertise({ data }) {
  const {
    header,
    ceo_block,
    banner_block,
    slider,
    news_sliders,
    supply_slides,
    case_preview,
  } = data;

  const SEO = {
    title: 'Expertise Codeska - IT Production Company',
    description: 'Expertise Codeska - IT Production Company',
  };
  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          imgSrc={header.image.url}
          hidden={header.hidden}
        />
      )}
      <main>
        {ceo_block && <Ceo ceo={ceo_block} company={false} />}
        <ExpertiseSlider data={supply_slides} />
        <Banner key={banner_block.id} data={banner_block} />
        {case_preview && <CasePreview data={case_preview} />}
        {news_sliders && <Slider data={news_sliders} news={true} />}
      </main>
    </>
  );
}

export async function getServerSideProps() {
  const res = await fetch(`${API_URL}/expertize`);
  const data = await res.json();
  return {
    props: {
      data,
    },
  };
}

export default Expertise;
