import Link from 'next/link';
import { useState, useEffect } from 'react';
import { NextSeo } from 'next-seo';

const SEO = {
  title: 'Codeska - 404',
  description: 'Codeska - 404',
};

const NoMatch = () => {
  return (
    <div className='container'>
      {/* <div className=''> */}
      <div className='no-match'>
        <h2 className='no-match__caption'>
          <strong>Sorry,</strong> this page isn't available
        </h2>
        <p className='no-match__text'>
          The page you were looking for couldn't be found. Perhaps you are
          looking for irrelevant information, try to return to the main page
        </p>

        <Link href='/'>
          <button className='btn' type='button'>
            <span>back to home</span>
          </button>
        </Link>
      </div>
      {/* </div> */}
    </div>
  );
};

export default NoMatch;
