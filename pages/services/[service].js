import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import PageDescription from 'components/PageDescription';
import Banner from 'components/Banner';
import CasePreview from 'components/CasePreview';
import Markdown from 'components/Markdown';

const { API_URL } = process.env;

const Service = ({ data }) => {
  const SEO = {
    title: `Service Codeska - ${data.title}`,
    description: `Service Codeska - ${data.title}`,
  };

  return (
    <>
      <NextSeo {...SEO} />
      {data.header && (
        <Header
          title={data.header.title}
          text={data.header.text}
          imgSrc={data.header.image.url}
          hidden={data.header.hidden}
        />
      )}
      <main>
        <article className='news-post section-margin-content'>
          {data.description && (
            <PageDescription
              title={data.description.title}
              text={data.description.text}
              hidden={data.description.hidden}
            />
          )}
          <div className='container'>
            <div className='row news-post__row'>
              <div className='col-sm-12 col-lg-12 news-post__image-wrapper image-wrapper'>
                <img
                  className='img-responsive img-responsive--cover'
                  src={data.main_image && `${API_URL}${data.main_image.url}`}
                  alt='article preview'
                />
              </div>
            </div>
            <div className='row news-post__row align-items-end'>
              <div className='col-sm-12 col-md-12 col-lg-8 news-post__col'>
                <div className='news-post__text-group'>
                  {data.main_image_paragraph &&
                    data.main_image_paragraph.map((item) => (
                      <p className='news-post__text' key={item.id}>
                        {item.text}
                      </p>
                    ))}
                </div>
                <div className='news-post__text-group'>
                  {data.advantages &&
                    data.advantages.length !== 0 &&
                    data.advantages.map((item, index) => {
                      if (!item.hidden) {
                        return (
                          <>
                            {index === 0 ? (
                              <h2 className='news-post__caption slug-service-caption caption caption--h2 caption--700'>
                                {item.title && item.title}
                              </h2>
                            ) : (
                              <h3 className='news-post__caption slug-service-caption caption caption--h3 caption--700'>
                                {item.title && item.title}
                              </h3>
                            )}
                            <p className='news-post__paragraph'>
                              {item.text && item.text}
                            </p>
                          </>
                        );
                      } else {
                        return null;
                      }
                    })}
                </div>
              </div>
              <aside className='col-lg-4 news-post__sidebar sidebar-service'>
                <div className='sidebar__item text-pannel'>
                  {data.first_sidebar_text && data.first_sidebar_text}
                </div>
              </aside>
            </div>
          </div>
          {data.banner_block && (
            <Banner key={data.banner_block.id} data={data.banner_block} />
          )}

          <div className='container'>
            <div className='row news-post__row align-items-end'>
              <div className='col-sm-12 col-md-12 col-lg-8 news-post__col'>
                <div className='news-post__text-group'>
                  <div className='news-post__image'>
                    {data.image && (
                      <img
                        className='img-responsive img-responsive--cover'
                        src={`${API_URL}${data.image.url}`}
                        alt='article preview'
                      />
                    )}
                  </div>
                  {!data.content.hidden && (
                    <>
                      <h3 className='news-post__caption caption caption--h3 caption--800'>
                        {data.content.title && data.content.title}
                      </h3>
                      {data.content.text.length !== 0 &&
                        data.content.text.map((item) => (
                          <p className='news-post__paragraph'>
                            {item.text && item.text}
                          </p>
                        ))}
                    </>
                  )}
                </div>
              </div>
              <aside className='col-lg-4 news-post__sidebar sidebar-service-sm'>
                <div className='sidebar__item text-pannel'>
                  {data.second_sidebar_text && data.second_sidebar_text}
                </div>
              </aside>
            </div>
          </div>
          <div className='container'>
            <Link href='/services'>
              <a className='show-more'>
                <button className='show-more__button'>
                  <span>go back to services</span>
                </button>
              </a>
            </Link>
          </div>
        </article>

        {data.case_preview && <CasePreview data={data.case_preview} />}
      </main>
    </>
  );
};

export async function getServerSideProps(context) {
  const res = await fetch(`${API_URL}/service-articles/${context.query.id}`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default Service;
