import Link from 'next/link';
import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import PageDescription from 'components/PageDescription';
import Banner from 'components/Banner';
import Slider from 'components/Slider';
import SwapSlider from 'components/SwapSlider';
import CasePreview from 'components/CasePreview';
const { API_URL } = process.env;
import parse from 'html-react-parser';
import { NextSeo } from 'next-seo';

const Services = ({ data }) => {
  const {
    header,
    description,
    banner,
    full_services,
    news_sliders,
    service_slider,
    case_preview,
  } = data;

  const SEO = {
    title: 'Services Codeska - IT Production Company',
    description: 'Services Codeska - IT Production Company',
  };

  const servicesBlock = () => {
    return (
      <section className='service'>
        <div className='container'>
          <div className='row'>
            {full_services.service_block.map((service) => {
              if (!service.hidden) {
                return (
                  <div
                    className='col-lg-6 service__item'
                    key={service.id && service.id}
                  >
                    <div className='service-kind-container'>
                      <span className='service__item-caption caption--700'>
                        {service?.title}
                      </span>
                      <div className='service__item-text'>
                        {parse(
                          service?.text.replace(/(?:\r\n|\r|\n)/g, '<br>')
                        )}
                      </div>
                      <div className='row service__row'>
                        <ul className='col-12 col-sm-12 col-lg-12 service__item-list list d-flex flex-wrap'>
                          {service?.item?.map((item, index) => {
                            return (
                              <li
                                key={item?._id}
                                className='list__item col-6 col-sm-6 col-lg-6'
                              >
                                {item?.point}
                              </li>
                            );
                          })}
                        </ul>
                        {/* <ul className='col-xs-6 col-sm-6 col-lg-6 service__item-list list'>
                          {service.item &&
                            service.item.map((item, index) => {
                              while (index < 3)
                                return (
                                  <li
                                    key={item?._id}
                                    className='list__item'
                                  >
                                    {item?.point}
                                  </li>
                                );
                            })}
                        </ul> */}
                        {/* <ul className='col-xs-6 col-sm-6 col-lg-6 service__item-list list'>
                          {service.item &&
                            service.item.map((item, index) => {
                              while (index > 2)
                                return (
                                  <li
                                    key={item?._id}
                                    className='list__item'
                                  >
                                    {item?.point}
                                  </li>
                                );
                            })}
                        </ul> */}
                      </div>
                      {service.service_article ? (
                        <Link
                          href={{
                            pathname: `${service.service_article}`
                              ? `/services/${service.title}`
                              : `/services`,
                            query: { id: service.service_article },
                          }}
                          // as={`/services/${service.title}`}
                        >
                          <a className='service-prev__link hero-link'>
                            read more
                          </a>
                        </Link>
                      ) : null}
                    </div>
                  </div>
                );
              } else {
                return null;
              }
            })}
          </div>
        </div>
      </section>
    );
  };

  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          main={true}
          ppt={header.ppt}
          imgSrc={header.image.url}
          hidden={header.hidden}
        />
      )}

      <main>
        {description && (
          <PageDescription
            title={description.title}
            text={description.text}
            hidden={description.hidden}
          />
        )}
        {full_services.service_block.length === 0 &&
        full_services.service_block.every((item) => item.hidden === true)
          ? null
          : full_services.service_block.length !== 0 &&
            full_services.service_block.every((item) => item.hidden === true)
          ? null
          : full_services.service_block.length !== 0 &&
            full_services.service_block.every((item) => item.hidden === false)
          ? servicesBlock()
          : servicesBlock()}
        {banner && <Banner data={banner} />}
        {service_slider ? (
          <SwapSlider
            data={service_slider.slide}
            hidden={service_slider.hidden}
            title={service_slider.title}
          />
        ) : null}
        {case_preview && <CasePreview data={case_preview} />}
        {news_sliders && <Slider data={news_sliders} news={true} />}
      </main>
    </>
  );
};

export async function getServerSideProps() {
  const res = await fetch(`${API_URL}/services`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default Services;
