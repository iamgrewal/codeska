import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from 'isomorphic-unfetch';
import InfoBar from 'components/Infobar';
import Banner from 'components/Banner';
import parse from 'html-react-parser';

const { API_URL } = process.env;

const NewsPost = ({ data }) => {
  const SEO = {
    title: `News Codeska - ${data.title}`,
    description: `News Codeska - ${data.title}`,
  };

  return (
    <>
      <NextSeo {...SEO} />
      <main>
        <article className='news-post'>
          <div className='container'>
            <div className=' d-flex  flex-column flex-md-row justify-content-between '>
              {data.infobar?.theme ? (
                <InfoBar
                  date={data.infobar.date ? data.infobar.date : null}
                  title={data.infobar?.theme}
                  commonClassName='news-post__info-bar'
                />
              ) : null}
              <Link href='/news'>
                <a className='show-more show-more--sub-news'>
                  <button className='show-more__button'>
                    <span className='span--sub-news'>go back</span>
                  </button>
                </a>
              </Link>
            </div>

            <div className='row news-post__row'>
              <div className='col news-post__wrapper'>
                <h1 className='news-post__caption caption caption--h1 caption--800'>
                  {data.title ? data.title : null}
                </h1>
                <p className='news-post__sub-caption sub-caption'>
                  {data.sub_title ? data.sub_title : null}
                </p>
              </div>
              <div className='col-sm-12 col-lg-12 news-post__image-wrapper image-wrapper'>
                <picture>
                  <source media='(min-width: 992px)' />
                  <img
                    className='img-responsive img-responsive--cover'
                    src={
                      data.main_image
                        ? `${API_URL}${data.main_image.url}`
                        : null
                    }
                    alt='article preview'
                  />
                </picture>
              </div>
            </div>
            <div className='row news-post__row'>
              <div className='col-sm-12 col-md-12 col-lg-8 news-post__col'>
                <div className='news-post__text-group'>
                  {data.paragraph
                    ? data.paragraph.map((item) => (
                        <p className='news-post__text' key={item.id}>
                          {item.text}
                        </p>
                      ))
                    : null}
                </div>
                <div className='news-post__text-group'>
                  <h2 className='news-post__caption caption caption--h2 caption--700'>
                    {data.second_title ? data.second_title : null}
                  </h2>
                  {data.paragraph_second
                    ? data.paragraph_second.map((item) => (
                        <p className='news-post__paragraph' key={item.id}>
                          {parse(item?.text.replace(/(?:\r\n|\r|\n)/g, '<br>'))}
                        </p>
                      ))
                    : null}
                </div>
              </div>
              <aside className='col-lg-4 news-post__sidebar sidebar'>
                <div className='sidebar__item text-pannel'>
                  {data.sidebar_text ? data.sidebar_text : null}
                </div>
                <div className='sidebar__item sidebar__item--image'>
                  <div className='sidebar__image-wrapper image-wrapper'>
                    {data?.sidebar_image ? (
                      <img
                        className='img-responsive'
                        src={
                          data.sidebar_image
                            ? `${API_URL}${data.sidebar_image.url}`
                            : ''
                        }
                        alt='sidebar-image'
                      />
                    ) : null}
                  </div>
                  <span className='sidebar__image-description'>
                    {data.sidebar_image_descript
                      ? data.sidebar_image_descript
                      : null}
                  </span>
                </div>
              </aside>
            </div>
          </div>
          {/* {data.banner_block ? (
            <Banner key={data.banner_block.id} data={data.banner_block} />
          ) : null} */}

          <div className='container'>
            <div className='row news-post__row'>
              <div className='col-sm-12 col-md-12 col-lg-8 news-post__col'>
                <div className='news-post__text-group'>
                  <div className='news-post__image'>
                    <picture>
                      <source media='(min-width: 992px)' />
                      <img
                        className='img-responsive img-responsive--cover'
                        src={data.image ? `${API_URL}${data.image.url}` : null}
                        alt='article preview'
                      />
                    </picture>
                  </div>
                  <h3 className='news-post__caption caption caption--h3 caption--800'>
                    {data?.third_title}
                  </h3>
                  <p className='news-post__paragraph'>
                    {parse(
                      data?.paragraph_third.replace(/(?:\r\n|\r|\n)/g, '<br>')
                    )}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </article>
        <div className='container'>
          <Link href='/news'>
            <a className='show-more show-more--news'>
              <button className='show-more__button'>
                <span>go back</span>
              </button>
            </a>
          </Link>
        </div>
      </main>
    </>
  );
};

export async function getServerSideProps(context) {
  const res = await fetch(`${API_URL}/news-articles/${context.query.id}`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default NewsPost;
