import { useRouter } from 'next/router';
import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import Banner from 'components/Banner';
import NewsPreview from 'components/NewsPreview';
const { API_URL } = process.env;
import { NextSeo } from 'next-seo';

function News({ data, news_articles, articles_number, article }) {
  const SEO = {
    title: 'News Codeska - IT Production Company',
    description: 'News Codeska - IT Production Company',
  };
  const router = useRouter();
  const { header, banner_block } = data;
  const lastNews = Math.ceil(articles_number / 1);
  console.log(news_articles.length);
  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          imgSrc={header.image.url}
          hidden={header.hidden}
        />
      )}

      <main>
        {news_articles.map((item, index) => {
          if (index <= 1)
            return (
              <NewsPreview
                key={item.id}
                // banner={banner_block}
                id={item.id}
                image={item.main_image}
                title={item.title}
                subTitle={item.sub_title}
                infobar={item.infobar}
              />
            );
        })}

        <Banner newsPage={true} key={banner_block.id} data={banner_block} />

        {news_articles.map((item, index) => {
          console.log(index);
          if (index >= 2)
            return (
              <NewsPreview
                key={item.id}
                // banner={banner_block}
                id={item.id}
                image={item.main_image}
                title={item.title}
                subTitle={item.sub_title}
                infobar={item.infobar}
              />
            );
        })}
        <div className='container'>
          {news_articles?.length > 5 && (
            <div className='show-more show-more--news'>
              <button
                className='show-more__button'
                onClick={() => router.push(`news?article=${article + 1}`)}
                disabled={article >= lastNews}
              >
                <span>show more news</span>
              </button>
            </div>
          )}
        </div>
      </main>
    </>
  );
}

export async function getServerSideProps({ query: { article = 4 } }) {
  const res = await fetch(`${API_URL}/news-page`);
  const numberOfArticles = await fetch(`${API_URL}/news-articles/count`);
  const res_articles = await fetch(
    `${API_URL}/news-articles?_limit=${article}`
  );
  const data = await res.json();
  const articles_number = await numberOfArticles.json();
  const news_articles = await res_articles.json();

  return {
    props: {
      data,
      news_articles,
      articles_number,
      article: +article,
    },
  };
}

export default News;
