import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-phone-input-2/lib/style.css';
import 'react-notifications-component/dist/theme.css';
import '../styles/style.scss';

import Head from 'next/head';
import { DefaultSeo } from 'next-seo';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import SEO from '../next-seo.config';
import ReactNotification from 'react-notifications-component';
import fetch from 'isomorphic-unfetch';
import getConfig from 'next/config';
import Menu from 'components/Menu';
import HeaderNav from 'components/Menu/Nav';
import Footer from 'components/Footer';

function MyApp({ Component, pageProps, footer, menu }) {
  const [path, setPath] = useState();
  const router = useRouter();

  useEffect(() => {
    if (router.pathname) {
      setPath(router.pathname);
    }
  }, []);
  return (
    <>
      <Head>
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      </Head>
      <DefaultSeo {...SEO} />
      <ReactNotification />
      <Menu menu={menu} path={path} />
      <div
        className='page-wrapper'
        style={
          path === '/404'
            ? {
                background: `url('/images/icons/404-bg.svg') no-repeat center center  fixed`,
                backgroundSize: 'cover',
                height: '100vh',
              }
            : { background: 'none' }
        }
      >
        <div className='container'>
          <div className='header__top-line'>
            <HeaderNav menu={menu} path={path} />
          </div>
        </div>
        <Component {...pageProps} />
        <footer
          style={path === '/404' ? { display: 'none' } : { display: 'block' }}
        >
          <Footer data={footer} menu={menu} />
        </footer>
      </div>
    </>
  );
}

const { publicRuntimeConfig } = getConfig();

MyApp.getInitialProps = async () => {
  const res_menu = await fetch(`${publicRuntimeConfig.API_URL}/menus`);
  const res_footer = await fetch(`${publicRuntimeConfig.API_URL}/footers`);
  // const res_news = await fetch(`${publicRuntimeConfig.API_URL}/news`);
  // const partners = await res.json();
  const footer = await res_footer.json();
  const menu = await res_menu.json();
  // const news = await res_news.json();

  return { footer, menu };
};

export default MyApp;
