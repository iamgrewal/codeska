import { useState } from 'react';
import Link from 'next/link';
const { API_URL } = process.env;
import { NextSeo } from 'next-seo';
import ApplyForm from 'components/Form';

const Vacancy = ({ data }) => {
  const { job_offer, position } = data;

  const SEO = {
    title: `Vacancy Codeska - ${position}`,
    description: `Vacancy Codeska - ${position}`,
  };
  return (
    <>
      <NextSeo {...SEO} />
      <main>
        <article className='offer-info'>
          <div className='container'>
            <div className='row'>
              <div className='col offer-info__wrapper'>
                <h1 className='offer-info__caption caption caption--h1 caption--800'>
                  {position ? position : null}
                </h1>
                <div className='offer-info__group'>
                  <span className='offer-info__group-title'>
                    {job_offer?.title_for_description}
                  </span>
                  <p className='offer-info__text'>{job_offer?.description}</p>
                </div>
                <div className='offer-info__group'>
                  <span className='offer-info__group-title'>
                    {job_offer?.title_for_skills}
                  </span>
                  <ul className='list'>
                    {job_offer?.skill.length !== 0
                      ? job_offer?.skill.map((el) => (
                          <li className='list__item' key={el.id ? el.id : null}>
                            {el.item ? el.item : null}
                          </li>
                        ))
                      : null}
                  </ul>
                </div>
                <div className='offer-info__group'>
                  <span className='offer-info__group-title'>
                    {job_offer?.title_for_responsibilities}
                  </span>
                  <ul className='list'>
                    {job_offer?.responsibility.length !== 0
                      ? job_offer?.responsibility.map((el) => (
                          <li className='list__item' key={el.id ? el.id : null}>
                            {el.item ? el.item : null}
                          </li>
                        ))
                      : null}
                  </ul>
                </div>

                <div className='offer-info__group'>
                  <span className='offer-info__group-title'>
                    {job_offer?.title_for_nice}
                  </span>
                  <ul className='list'>
                    {job_offer?.nice_to_have.length !== 0
                      ? job_offer?.nice_to_have.map((el) => (
                          <li className='list__item' key={el.id}>
                            {el.item}
                          </li>
                        ))
                      : null}
                  </ul>
                </div>
                <div className='offer-info__group'>
                  <span className='offer-info__group-title'>
                    {job_offer?.title_for_offer}
                  </span>
                  <ul className='list'>
                    {job_offer?.proposal.length !== 0
                      ? job_offer?.proposal.map((el) => (
                          <li className='list__item' key={el.id}>
                            {el.item}
                          </li>
                        ))
                      : null}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </article>
        <div className='container mb-120'>
          <Link href='/careers'>
            <a className='show-more'>
              <button className='show-more__button'>
                <span>back to vacancies</span>
              </button>
            </a>
          </Link>
        </div>
        <div className='container'>
          <div className='row'>
            <div className='col-6 vacancy-form-wrapper section-margin-content-sm'>
              <ApplyForm vacancyForm={true} careerForm={true} />
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export async function getServerSideProps(context) {
  const res = await fetch(`${API_URL}/vacancies/${context.query.id}`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default Vacancy;
