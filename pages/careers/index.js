import { useState, useCallback } from 'react';
import fetch from 'isomorphic-unfetch';
import Header from 'components/Header';
import PageDescription from 'components/PageDescription';
import InfoBar from 'components/Infobar';
import CareerPosition from 'components/CareerPosition';
import Slider from 'components/Slider';
import Csr from 'components/Csr';
import Popup from 'components/Popup';
import { NextSeo } from 'next-seo';
const { API_URL } = process.env;

function Careers({ data }) {
  const SEO = {
    title: 'Careers Codeska - IT Production Company',
    description: 'Careers Codeska - IT Production Company',
  };
  const {
    header,
    button_text,
    description,
    vacancies_list,
    news_sliders,
    csr_block,
  } = data;

  const [modalIsOpen, setModalIsOpen] = useState(false);
  const openModal = () => {
    setModalIsOpen(true);
  };

  return (
    <>
      <NextSeo {...SEO} />
      {header && (
        <Header
          title={header.title}
          text={header.text}
          imgSrc={header.image.url}
          career={true}
          main={true}
          button_text={button_text}
          openModal={openModal}
          hidden={header.hidden}
        />
      )}

      <main>
        {description && (
          <PageDescription
            title={description.title}
            text={description.text}
            hidden={description.hidden}
          />
        )}
        <section className='job-positions'>
          <div className='container'>
            {vacancies_list.length !== 0 ? (
              <InfoBar date='' title='vacancies' commonClassName='' />
            ) : null}
            <div className='row'>
              {vacancies_list.length !== 0
                ? vacancies_list.map((item) => (
                    <CareerPosition
                      key={item.id ? item.id : null}
                      kind={item.who ? item.who : null}
                      position={item.position ? item.position : null}
                      job_offer_id={item.id ? item.id : null}
                    />
                  ))
                : null}
            </div>
          </div>
        </section>
        {csr_block && <Csr csr={csr_block} company={true} />}
        {news_sliders && <Slider data={news_sliders} news={true} />}
        <Popup
          isOpen={modalIsOpen}
          setModalState={setModalIsOpen}
          careerForm={true}
        />
      </main>
    </>
  );
}

export async function getServerSideProps() {
  const res = await fetch(`${API_URL}/careers`);
  const data = await res.json();
  return {
    props: {
      data,
    },
  };
}

export default Careers;
