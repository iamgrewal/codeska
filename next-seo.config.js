export default {
  title: "Testing Website with Headless",
  description: "Testing Website with Headless",
  openGraph: {
    type: "website",
    locale: "en_IE",
    url: "https://test02.grewal.co",
    site_name: "Testing Website with Headless",
  },
  // twitter: {
  //     handle: '@iamgrewal',
  //     site: '@iamgrewal',
  //     cardType: 'summary_large_image',
  // },
};
