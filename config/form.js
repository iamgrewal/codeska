export const isEmpty = (value = "") => !value || !value.trim();

export const isEmail = (value) => {
  // eslint-disable-next-line max-len,no-useless-escape
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(value).toLowerCase());
};

export const isName = (value) => {
  const re = /^([A-Z][a-z]{3,} )([A-Z][a-z]{3,} )?([A-Z][a-z]{3,})$/;
  return re.test(String(value).toLowerCase());
};

export const cleanFormErrors = (formErrors = {}) => {
  if (Object.values(formErrors).find((error) => error)) {
    return formErrors;
  }
  return {};
};
