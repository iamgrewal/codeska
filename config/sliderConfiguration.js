const sliderConfiguration = {
  type: `carousel`,
  startAt: 0,
  gap: 0,
  animationDuration: 600,
  focusAt: `center`,
  perView: 1,
  afterTransition: function (event) {
    console.log(event.index); // the current slide number
  },
};

export default sliderConfiguration;
