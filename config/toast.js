import { store as toastNotification } from 'react-notifications-component';

export const defaultNotificationSetting = {
  insert: `bottom`,
  container: `bottom-right`,
  animationIn: [`animated`, `fadeIn`],
  animationOut: [`animated`, `fadeOut`],
  dismiss: {
    // duration: 50000,
    pauseOnHover: true, // if true - fading out will be paused on hover
    waitForAnimation: true, // if true, animation fading out to initial state is absent
  },
};

const applyToast = ({ type, text }) => {
  return toastNotification.addNotification({
    ...defaultNotificationSetting,
    type,
    message: `${text}`,
    dismiss: {
      duration: 4000,
    },
  });
};

export default applyToast;
