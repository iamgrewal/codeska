import { useEffect, useRef } from 'react';
import Link from 'next/link';
import Glide from '@glidejs/glide';
import sliderConfiguration from '../../config/sliderConfiguration';
import { useRouter } from 'next/router';
import parse from 'html-react-parser';
const { API_URL } = process.env;

const ExpertiseSlider = ({ data }) => {
  const router = useRouter();
  const ref = useRef();
  const trackRef = useRef();

  useEffect(() => {
    const slider = new Glide(ref.current, sliderConfiguration);
    slider.on('build.after', function () {
      const nodes = trackRef?.current.childNodes[0].childNodes;
      const targetItem = Array.from(nodes).find((item) =>
        item.classList.contains('glide__slide--active')
      );
      const activeSlideHeight = targetItem?.clientHeight;
      if (activeSlideHeight)
        trackRef.current.style.height = activeSlideHeight + 'px';
    });
    slider.on('run.after', function () {
      const nodes = trackRef.current.childNodes[0].childNodes;
      const targetItem = Array.from(nodes).find((item) =>
        item.classList.contains('glide__slide--active')
      );
      const activeSlideHeight = targetItem?.clientHeight;
      if (activeSlideHeight)
        trackRef.current.style.height = activeSlideHeight + 'px';
    });
    slider.mount();
  }, [router, ref, trackRef]);
  return (
    <>
      <section className='supply-schema '>
        <div
          className='container glide glide--ltr glide--slider glide--swipeable'
          ref={ref}
        >
          <div className='supply-schema__header'>
            <h2 className='supply-schema__caption caption caption--800'>
              IT solutions for Supply Chain
            </h2>
            <div className={'supply-schema__chain'}>
              <div className='supply-schema__line'></div>
              {/* BUTTONS WRAPPER */}
              <div className='supply-schema__wrapper'>
                <div
                  className='supply-schema__line-inner glide__bullets'
                  style={{ width: '100%' }}
                  data-glide-el='controls[nav]'
                >
                  {data &&
                    data.map((item, index) => {
                      if (!item?.hidden) {
                        return (
                          <button
                            key={index}
                            className='supply-schema__line-item glide__bullet'
                            data-glide-dir={`=${index}`}
                          >
                            <span className={'supply-schema__descr'}>
                              {item?.slide_name}
                            </span>
                          </button>
                        );
                      } else {
                        return null;
                      }
                    })}
                </div>
              </div>
            </div>
            {/* SLIDER TRACK */}
            <div
              ref={trackRef}
              className='supply-schema__track glide__track'
              data-glide-el='track'
              style={{ transition: 'all 0.7s' }}
            >
              {/* SLIDES */}
              <div className='glide__slides'>
                {data &&
                  data.map((elem, index) => {
                    let id = elem?.expertise_article;
                    if (!elem.hidden) {
                      return (
                        <div className='glide__slide row' key={index}>
                          <div className='col-lg-6 stage'>
                            <h3 className='stage__caption caption caption--800'>
                              {elem?.slide_title}
                            </h3>
                            {elem?.read_more_text && (
                              <Link
                                href={{
                                  pathname: `${elem?.expertise_article}`
                                    ? `/expertise/expertise_article`
                                    : '/expertise',
                                  query: { id: id },
                                }}
                              >
                                <a className='hero-link mb-20'>
                                  {elem.read_more_text}
                                </a>
                              </Link>
                            )}
                            <p className='d-none d-md-block stage__text text text--regular'>
                              {parse(
                                elem?.text.replace(/(?:\r\n|\r|\n)/g, '<br>')
                              )}
                            </p>

                            <div className='row'>
                              <div className='col'>
                                <span className='stage__group-title stage__group-title--supply'>
                                  {elem?.first_title}
                                </span>
                                <ul className='stage__list list'>
                                  {elem?.items?.map((point, index) => (
                                    <li className='list__item' key={index}>
                                      {point?.item}
                                    </li>
                                  ))}
                                </ul>
                                <span className='stage__group-title stage__group-title--supply'>
                                  {elem?.second_title}
                                </span>
                                <ul className='stage__list list'>
                                  {elem?.items_two.map((point, index) => (
                                    <li className='list__item' key={index}>
                                      {point?.item}
                                    </li>
                                  ))}
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div className='col-lg-6 supply-schema__image-wrapper'>
                            <img
                              className='img-responsive img-responsive--cover'
                              src={`${API_URL}${elem?.image?.url}`}
                              style={{ maxHeight: '100%' }}
                              alt='Supply Chain'
                            />
                            {elem?.ppt && (
                              <a
                                className='hero-link supply-schema__ppt'
                                target='_blank'
                              >
                                download presentation
                              </a>
                            )}
                          </div>
                        </div>
                      );
                    } else {
                      return null;
                    }
                  })}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ExpertiseSlider;
