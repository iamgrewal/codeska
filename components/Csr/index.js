import Link from 'next/link';
import Markdown from '../Markdown';
import InfoBar from '../Infobar';

const { API_URL } = process.env;

const Csr = ({ csr, company }) => {
  return (
    <>
      {!csr.hidden ? (
        <section className='ceo'>
          <div className='container'>
            {csr?.infobar?.theme ? (
              <InfoBar
                date=''
                title={csr.infobar.theme}
                commonClassName=''
                hrefLink='/mission'
              />
            ) : null}
            <div className='row justify-content-between'>
              <div className='col-md-6 col-lg-6 col-xl-6'>
                <Markdown className='ceo__slogan' source={csr?.slogan} />
                <div className='ceo__person'>
                  <Markdown
                    className='ceo__caption caption caption--h3 ceo__caption--csr'
                    source={csr?.title}
                  />
                  {csr?.paragraph?.map((item) => (
                    <Markdown
                      className='ceo__text'
                      key={item?.id}
                      source={item?.text}
                    />
                  ))}
                  {company && csr?.link_text ? (
                    <Link href='/mission'>
                      <a className='ceo__link hero-link'>{csr.link_text}</a>
                    </Link>
                  ) : null}
                </div>
              </div>
              <div className='col-sm-12 col-md-6 col-lg-5 col-xl-5 expertise__image-wrapper'>
                <img
                  className='img-responsive img-responsive--cover'
                  src={csr?.image?.url && `${API_URL}${csr.image.url}`}
                  alt='mission of Codeska'
                />
              </div>
            </div>
          </div>
        </section>
      ) : null}
    </>
  );
};

export default Csr;
