const TextAreaAdapter = ({ input, meta, className, placeholder }) => {
  return (
    <>
      <textarea
        {...input}
        type={input.type}
        name={input.name}
        maxLength='2000'
        className={
          meta.error && meta.touched
            ? `${className} error`
            : meta.valid
            ? `${className} agreed`
            : `${className}`
        }
        placeholder={placeholder}
      />
    </>
  );
};

export default TextAreaAdapter;
