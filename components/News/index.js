import { useEffect, useRef } from 'react';
import Glide from '@glidejs/glide';
import sliderConfiguration from '../../config/sliderConfiguration';
const { API_URL } = process.env;

const News = ({ news }) => {
  const ref = useRef();

  useEffect(() => {
    const slider = new Glide(ref.current, sliderConfiguration);
    slider.mount();
  }, [ref]);

  return (
    <section className='supply-schema'>
      <div className='container glide ' ref={ref}>
        <div className='supply-schema__header'>
          <h2 className='supply-schema__caption caption caption--800'>
            IT solutions for Supply Chain
          </h2>
          <div className='supply-schema__chain'>
            <div className='supply-schema__line'></div>
            <div
              className='supply-schema__line-inner glide__bullets'
              data-glide-el='controls[nav]'
            >
              {news.map((item, index) => {
                return (
                  <button
                    className='supply-schema__line-item glide__bullet'
                    data-glide-dir={`=${index}`}
                  >
                    <span>{item.slide_name}</span>
                  </button>
                );
              })}
              <button className='supply-schema__line-item glide__bullet'>
                <span>more news</span>
              </button>
            </div>
          </div>
          {/* slide track */}
          <div
            ref={ref}
            className='supply-schema__track glide__track'
            data-glide-el='track'
          >
            <div className='glide__slides'>
              {news.map((elem) => {
                return (
                  <div className='row glide__slide'>
                    <div className='col-lg-6 stage'>
                      <h3 className='stage__caption caption caption--h1 caption--800'>
                        {elem.intro.title}
                      </h3>
                      <p className='stage__text text text--regular'>
                        {elem.intro.text}
                      </p>
                      <div className='row'>
                        <div className='col'>
                          <span className='stage__group-title'>
                            {elem.highlight}
                          </span>
                          <ul className='stage__list list'>
                            {elem.item.map((el) => (
                              <li className='list__item' key={el.id}>
                                {el.item}
                              </li>
                            ))}
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className='col-lg-6 supply-schema__image-wrapper'>
                      <img
                        className='img-responsive img-responsive--cover'
                        src={`${API_URL}${elem.image.url}`}
                        alt='news image'
                      />
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default News;
