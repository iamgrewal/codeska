import Link from 'next/link';
import InfoBar from '../Infobar';
import Markdown from '../Markdown';

const { API_URL } = process.env;
const News = ({ data, allcases }) => {
  return (
    <>
      {data.length !== 0 ? (
        <section className='blog-posts section-margin'>
          <div className='container'>
            {data.length !== 0 ? (
              <InfoBar date='' title='cases' commonClassName='' />
            ) : null}
            <div className='row blog-posts__row flex-sm-column flex-md-row'>
              {allcases && data.length !== 0
                ? data.map((item) => (
                    <div
                      className='col-12 col-lg-6 col-xl-6 blog-posts__col'
                      key={item.id}
                    >
                      <div className='blog-post-prev__wrapper'>
                        <div className='blog-post-prev'>
                          <img
                            className='img-responsive img-responsive--cover blog-post-prev__image'
                            src={
                              item?.background?.url &&
                              `${API_URL}${item?.background?.url}`
                            }
                            alt='image'
                          />
                          {item?.logo?.url && (
                            <div className='blog-post-prev__container'>
                              <img
                                className='blog-post-prev__logo'
                                src={
                                  item?.logo?.url &&
                                  `${API_URL}${item.logo.url}`
                                }
                                alt='logo'
                              />
                            </div>
                          )}
                        </div>
                        {console.log(item)}
                        <Link
                          href={{
                            pathname: `${item.case_article}`
                              ? `/cases/${item.name}`
                              : '/cases',
                            query: { id: item.case_article },
                          }}
                          // as={`/cases/${item.name}`}
                        >
                          <a className=''>
                            <Markdown
                              className='blog-post-prev__caption mb-20'
                              source={item.subtext && item.subtext}
                            />
                          </a>
                        </Link>

                        <Markdown
                          className='blog-post-prev__text'
                          source={item.text && item.text}
                        />
                      </div>
                    </div>
                  ))
                : data.slice(0, 2).map((item) => (
                    <div
                      className='col-12 col-md-6 blog-posts__col'
                      key={item.id}
                    >
                      <Link
                        href={{
                          pathname: `${item.case_article}`
                            ? `/cases/case`
                            : '/',
                          query: { id: item.case_article },
                        }}
                        // as={`/cases/${item.name}`}
                      >
                        <a>
                          <div className='blog-post-prev__wrapper'>
                            <div className='blog-post-prev'>
                              <img
                                className='img-responsive img-responsive--cover blog-post-prev__image'
                                src={
                                  item.background.url &&
                                  `${API_URL}${item.background.url}`
                                }
                                alt='image'
                              />
                              <div className='blog-post-prev__container'>
                                <img
                                  className='blog-post-prev__logo'
                                  src={
                                    item.logo.url &&
                                    `${API_URL}${item.logo.url}`
                                  }
                                  alt='logo'
                                />
                              </div>
                            </div>
                            <Markdown
                              className='blog-post-prev__caption'
                              source={item.subtext && item.subtext}
                            />
                          </div>
                        </a>
                      </Link>
                    </div>
                  ))}
            </div>
            {!allcases && data.length !== 0 ? (
              <a className='ceo__link hero-link' href='/cases'>
                more cases
              </a>
            ) : null}
          </div>
        </section>
      ) : null}
    </>
  );
};

export default News;
