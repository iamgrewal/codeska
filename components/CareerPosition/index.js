import Link from 'next/link';

const CareerPosition = ({ kind, position, job_offer_id }) => {
  return (
    <div className='col-lg-4 job-positions__group'>
      <Link
        href={{ pathname: `/careers/${position}`, query: { id: job_offer_id } }}
      >
        <a className='position' target='_blank'>
          <span className='position__item position__item--kind'>{kind}</span>
          <span className='position__item position__item--profession'>
            {position}
          </span>
        </a>
      </Link>
    </div>
  );
};

export default CareerPosition;
