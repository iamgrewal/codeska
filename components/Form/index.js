import { useState, useCallback } from 'react';
import fetch from 'isomorphic-unfetch';
import { Form, Field } from 'react-final-form';
const { API_URL } = process.env;
import { cleanFormErrors, isEmail, isName, isEmpty } from '../../config/form';
import FormInputAdapter from 'components/FormInputAdapter';
import FormPhoneAdapter from 'components/FormPhoneAdapter';
import TextAreaAdapter from 'components/TextAreaAdapter';
import CheckBox from 'components/CheckBox';
import applyToast from '../../config/toast';
import Popup from 'components/Popup';

const ApplyForm = ({ careerForm, vacancyForm, closeModal }) => {
  const [identityChecked, setIdentityChecked] = useState(false);
  const onSubmit = useCallback((data) => {
    fetch(careerForm ? `${API_URL}/form-vacancies` : `${API_URL}/forms`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then((res) => {
        // console.log(res);
        applyToast({
          type: 'success',
          text: 'Yor message was sucessfully sent!',
        });
        closeModal();
      })
      .catch((e) => {
        console.error(e);
        applyToast({
          type: 'error',
          text: 'Something went wrong. Plase try agan',
        });
      });
  });

  const validate = useCallback((data) => {
    const { name, email, company, link, phone, message } = data;
    const errorsContact = {
      name: isEmpty(name),
      email: !isEmail(email),
      company: isEmpty(company),
      phone: isEmpty(phone),
      message: isEmpty(message),
    };
    const errorsCareer = {
      name: isEmpty(name),
      email: !isEmail(email),
      link: isEmpty(link),
      phone: isEmpty(phone),
      message: isEmpty(message),
    };

    return cleanFormErrors(careerForm ? errorsCareer : errorsContact);
  }, []);

  return (
    <Form
      onSubmit={onSubmit}
      validate={validate}
      render={({ handleSubmit, invalid }) => (
        <form className='contact-popup__form' onSubmit={handleSubmit}>
          {vacancyForm ? (
            <div>
              <h2 className='contact-popup__caption caption--h2 caption--700'>
                Is this what you need?
              </h2>
              <p className='contact-popup__text'>
                If you liked the vacancy, and you feel that you can be useful,
                then hurry up, fill out the application and we will consider
                your resume.
              </p>
            </div>
          ) : (
            <div>
              <h2 className='contact-popup__caption caption--h2 caption--700'>
                Let's work together
              </h2>
              <p className='contact-popup__text'>
                Codeska cares about ypur company values and are ready to share
                their experience. Send us your details and we will contact you
              </p>
            </div>
          )}

          <div className='contact-popup__form-body'>
            <div className='contact-popup__text-row align-items-start'>
              <div className='contact-popup__text-field contact-popup__half-width popup-field'>
                <Field
                  className='popup-field__input'
                  name='name'
                  type='text'
                  component={FormInputAdapter}
                  placeholder='name*'
                />
              </div>
              <div className='contact-popup__text-field contact-popup__half-width popup-field'>
                <Field
                  className='popup-field__input popup-field__input--email'
                  name='email'
                  type='email'
                  component={FormInputAdapter}
                  placeholder='email*'
                />
                <p className='popup-field__subtext'>
                  Enter the current email. At the specified address, you will
                  receive a response from us.
                </p>
              </div>
            </div>
            <div className='contact-popup__text-row'>
              <div className='contact-popup__text-field contact-popup__half-width popup-field'>
                <Field
                  className='popup-field__input popup-field__input--phone'
                  name='phone'
                  type='tel'
                  meta
                  component={FormPhoneAdapter}
                />
              </div>
              <div className='contact-popup__text-field contact-popup__half-width popup-field'>
                <Field
                  className='popup-field__input'
                  name={careerForm ? 'link' : 'company'}
                  type='text'
                  component={FormInputAdapter}
                  placeholder={
                    careerForm ? 'Link to your CV*' : 'company name*'
                  }
                />
              </div>
            </div>

            <div className='contact-popup__text-row'>
              <div className='contact-popup__text-field popup-field'>
                <Field
                  name='message'
                  className='popup-field__textarea'
                  type='text'
                  component={TextAreaAdapter}
                  placeholder={
                    careerForm
                      ? 'Cover letter'
                      : 'What can we help you with? (You can also add links to this field)'
                  }
                />
              </div>
            </div>
            <div className='contact-popup__text-row'>
              <div className='contact-popup__text-field contact-popup__half-width popup-field'>
                <button
                  type='submit'
                  className='popup-field__input popup-field__input--btn'
                  disabled={invalid || !identityChecked ? true : false}
                >
                  send
                </button>
              </div>
              <div className='contact-popup__text-field contact-popup__half-width popup-field d-flex '>
                <CheckBox
                  checked={identityChecked}
                  onChange={setIdentityChecked}
                />
                <span className='contact-popup__text-policy'>
                  I agree to the{' '}
                  <a
                    rel='noreferrer'
                    href='https://www.privacypolicygenerator.info/live.php?token=mCaUDkNZ3CSCLKOSE7Q9UOYjlcHpV3nu'
                    target='_blank'
                  >
                    Privacy Policy
                  </a>{' '}
                  and{' '}
                  <a
                    rel='noreferrer'
                    href='https://www.cookiepolicygenerator.com/live.php?token=0bHyDNMeIsWLeHuIQ9trEhBvFVuyVs5r'
                    target='_blank'
                  >
                    Cookies Policy
                  </a>{' '}
                  of this website
                </span>
              </div>
            </div>
          </div>
        </form>
      )}
    />
  );
};

export default ApplyForm;
