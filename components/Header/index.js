import { Col } from 'react-bootstrap';
import Markdown from '../Markdown';

const Header = ({
  main,
  title,
  text,
  imgSrc,
  ppt,
  button_text,
  career,
  openModal,
  currentCase,
  hidden,
}) => {
  const { API_URL } = process.env;

  return (
    <>
      {!hidden ? (
        <header className={main ? 'header header--main' : 'header'}>
          <div className='container'>
            <div className='row flex-sm-column justify-content-sm-center flex-md-row justify-content-md-between header__bottom-line'>
              <div
                className={
                  main
                    ? 'col-md-8 col-lg-8 col-xl-8 header__col-text'
                    : 'col-md-5 col-lg-6 col-xl-6 header__col-text'
                }
              >
                <Markdown
                  className='header__caption caption caption--h1 caption--800'
                  source={title && title}
                />
                <Markdown
                  className={
                    `${currentCase}`
                      ? 'header__sub-caption sub-caption case-header'
                      : 'header__sub-caption sub-caption '
                  }
                  source={text}
                />
                {ppt && (
                  <a
                    className='header__link hero-link'
                    target='_blank'
                    href={`${API_URL}${ppt.url}`}
                  >
                    download presentation
                  </a>
                )}
                {career && button_text ? (
                  <button
                    className='btn'
                    type='button'
                    onClick={() => openModal()}
                  >
                    <span>{button_text}</span>
                  </button>
                ) : null}
              </div>
              <div
                className={
                  main
                    ? 'col-md-4 col-lg-4 col-xl-4'
                    : 'col-md-7 col-lg-6 col-xl-6'
                }
              >
                <div className='header__image-wrapper'>
                  <img
                    className='img-responsive'
                    src={imgSrc && `${API_URL}${imgSrc}`}
                    alt='page-preview'
                  />
                </div>
              </div>
            </div>
          </div>
        </header>
      ) : null}
    </>
  );
};

export default Header;
