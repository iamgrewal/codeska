import { useState } from 'react';
const { API_URL } = process.env;
import Markdown from '../Markdown';
import Popup from 'components/Popup';

const Banner = ({ data, newsPage }) => {
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const openModal = () => {
    setModalIsOpen(true);
  };

  return (
    <section className={`banner ${newsPage ? 'mb-120' : 'section-margin'}`}>
      {data ? (
        <div className='container'>
          <div className='row banner__row'>
            <div className='col-md-6 col-lg-6 col-xl-8 banner__col'>
              <Markdown
                className='banner__caption caption caption--h2 caption--800'
                source={data.title ? data.title : null}
              />
              <Markdown
                className='banner__description'
                source={data.text ? data.text : null}
              />
              <button
                className='banner__btn btn'
                type='button'
                onClick={openModal}
              >
                <span>{data.button_text ? data.button_text : null}</span>
              </button>
            </div>
            <div className='col-md-6 col-lg-6 col-xl-4'>
              <div className='banner__image-wrapper'>
                <img
                  src={data.image.url ? `${API_URL}${data.image.url}` : null}
                  alt='banner image'
                />
              </div>
            </div>
          </div>
        </div>
      ) : null}

      <Popup isOpen={modalIsOpen} setModalState={setModalIsOpen} />
    </section>
  );
};
export default Banner;
