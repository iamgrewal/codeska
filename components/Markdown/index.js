import ReactMarkdown from "react-markdown";

const Markdown = ({ className, source }) => {
  return (
    <ReactMarkdown
      className={className}
      source={source}
    />
  );
};

export default Markdown;
