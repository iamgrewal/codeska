import { useEffect, useRef } from 'react';
import Glide from '@glidejs/glide';
import { useRouter } from 'next/router';
import sliderConfiguration from '../../config/sliderConfiguration';
const { API_URL } = process.env;

const Slider = ({ data, hidden, title }) => {
  const router = useRouter();
  const ref = useRef();
  const trackRef = useRef();

  useEffect(() => {
    const slider = new Glide(ref.current, sliderConfiguration);
    if (router?.pathname === '/services') {
      slider.on('build.after', function () {
        const nodes = trackRef?.current.childNodes[0].childNodes;
        const targetItem = Array.from(nodes).find((item) =>
          item.classList.contains('glide__slide--active')
        );
        const activeSlideHeight = targetItem?.clientHeight;
        if (activeSlideHeight)
          trackRef.current.style.height = activeSlideHeight + 'px';
      });
      slider.on('run.after', function () {
        const nodes = trackRef.current.childNodes[0].childNodes;
        const targetItem = Array.from(nodes).find((item) =>
          item.classList.contains('glide__slide--active')
        );
        const activeSlideHeight = targetItem?.clientHeight;
        if (activeSlideHeight)
          trackRef.current.style.height = activeSlideHeight + 'px';
      });
      slider.mount();
    }
  }, [router, ref, trackRef]);

  return (
    <>
      {!hidden ? (
        <section className='processes'>
          <div className='container'>
            <h2 className='processes__caption caption caption--h2 caption--800'>
              {title && title}
            </h2>
            <div className='processes__wrapper glide' ref={ref}>
              <div className='swiper-container swiper1'>
                <div
                  className='swiper-wrapper glide__bullets'
                  data-glide-el='controls[nav]'
                >
                  {data &&
                    data.map((item, index) => (
                      <div
                        className='swiper-slide glide__bullet'
                        key={item.id}
                        data-glide-dir={`=${index}`}
                      >
                        <span>{item.button.title}</span>
                        <p>{item.button.text && item.button.text}</p>
                      </div>
                    ))}
                </div>
              </div>
              <div className='swiper-container swiper2 '>
                <div
                  ref={trackRef}
                  className='swiper-wrapper glide__track'
                  data-glide-el='track'
                  style={{ transition: 'all 0.7s' }}
                >
                  <div className='glide__slides'>
                    {data &&
                      data.map((item, index) => (
                        <div
                          className='swiper-slide tab-slide glide__slide'
                          key={index}
                        >
                          <div className='tab-slide__header'>
                            <h2 className='tab-slide__caption caption caption--h3 caption--700'>
                              {item.slide_title && item.slide_title}
                            </h2>
                            <p className='tab-slide__about'>
                              {item.slide_about && item.slide_about}
                            </p>
                          </div>
                          <div className='tab-slide__image-wrapper'>
                            <img
                              className='img-responsive img-responsive--cover'
                              src={
                                item.image.url && `${API_URL}${item.image.url}`
                              }
                              alt='business process image'
                            />
                          </div>
                          <div className='tab-slide__body'>
                            <p className='tab-slide__paragraph'>
                              {item.slide_paragraph_one &&
                                item.slide_paragraph_one}
                            </p>
                            <p className='tab-slide__paragraph'>
                              {item.slide_paragraph_two &&
                                item.slide_paragraph_two}
                            </p>
                          </div>
                          <div className='tab-slide__footer'>
                            <span className='tab-slide__sub-caption'>
                              {item.slide_sub_caption && item.slide_sub_caption}
                            </span>
                            <p className='tab-slide__paragraph'>
                              {item.slide_paragraph_three &&
                                item.slide_paragraph_three}
                            </p>
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      ) : null}
    </>
  );
};

export default Slider;
