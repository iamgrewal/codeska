import { useEffect, useRef, useState } from 'react';
import Link from 'next/link';
import Glide from '@glidejs/glide';
import sliderConfiguration from '../../config/sliderConfiguration';
import InfoBar from '../Infobar';
const { API_URL } = process.env;

const Slider = ({ data, news }) => {
  const ref = useRef();

  useEffect(() => {
    const slider = new Glide(ref.current, sliderConfiguration);
    if (slider && ref.current) {
      slider.mount();
    }
  }, [ref]);

  // const checkIfSlidesHidden = () => {
  //   // console.log(data);
  //   // data.every(function checkValue(item) {
  //   //   return item.hidden === true
  //   // })
  //   data.every((item) => console.log(item.hidden === true));
  // };

  // checkIfSlidesHidden();

  return (
    <>
      {data && data.length !== 0 ? (
        <>
          <div className='container'>
            {news && <InfoBar date='' title='news' commonClassName='' />}
          </div>
          <section className='supply-schema'>
            <div
              className='container glide glide--ltr glide--slider glide--swipeable'
              ref={ref}
            >
              <div className='supply-schema__header'>
                <div
                  className={'supply-schema__chain supply-schema__chain--news'}
                >
                  <div className='supply-schema__line'></div>
                  {/* BUTTONS WRAPPER */}
                  <div className='supply-schema__wrapper'>
                    <div
                      className='supply-schema__line-inner glide__bullets'
                      style={{ width: '70%' }}
                      data-glide-el='controls[nav]'
                    >
                      {data &&
                        data.map((item, index) => {
                          if (!item.hidden) {
                            return (
                              <button
                                key={index}
                                className='supply-schema__line-item glide__bullet'
                                data-glide-dir={`=${index}`}
                              >
                                <span
                                  className={
                                    'supply-schema__descr supply-schema__descr--news'
                                  }
                                >
                                  {item.slide_name}
                                </span>
                              </button>
                            );
                          } else {
                            return null;
                          }
                        })}
                    </div>
                    {/* BUTTON IS VISIBLE IF SLIDER IS FOR NEWS */}
                    {news && (
                      <Link href='/news'>
                        <a className='supply-schema__line-item glide__bullet glide__bullet--fake'>
                          <button>
                            <span className='supply-schema__descr supply-schema__descr--fake'>
                              more news
                            </span>
                          </button>
                        </a>
                      </Link>
                    )}
                  </div>
                </div>
                {/* SLIDER TRACK */}
                <div
                  className='supply-schema__track glide__track'
                  data-glide-el='track'
                >
                  {/* SLIDES */}
                  <div className='glide__slides'>
                    {data &&
                      data.map((elem, index) => {
                        let id = elem.news_article;
                        console.log(id);
                        if (!elem.hidden) {
                          return (
                            <div className='row glide__slide' key={index}>
                              <div className='col-lg-6 stage'>
                                {news && elem.news_article ? (
                                  <Link
                                    href={{
                                      pathname: `${elem.news_article}`
                                        ? `/news/${elem.intro.title}`
                                        : '/',
                                      query: { id: id },
                                    }}
                                    // as={`/news/${elem.intro.title}`}
                                  >
                                    <a
                                      className='stage__caption caption caption--h1 caption--800'
                                      // as={`/news/${elem.intro.title}`}
                                    >
                                      {elem.intro.title}
                                    </a>
                                  </Link>
                                ) : (
                                  <h3 className='stage__caption caption caption--800'>
                                    {elem.intro.title}
                                  </h3>
                                )}

                                <p className='d-none d-md-block stage__text text text--regular'>
                                  {elem.intro.text}
                                </p>
                              </div>
                              <div className='col-lg-6 supply-schema__image-wrapper'>
                                {news && elem.news_article ? (
                                  <Link
                                    href={{
                                      pathname: `${elem.news_article}`
                                        ? `/news/news-post`
                                        : '',
                                      query: { id: id },
                                    }}
                                    // as={`/news/${elem.intro.title}`}
                                  >
                                    <a>
                                      <img
                                        className='img-responsive img-responsive--cover'
                                        src={`${API_URL}${elem.image.url}`}
                                        style={
                                          news
                                            ? { maxHeight: '70%' }
                                            : { maxHeight: '100%' }
                                        }
                                        alt='slider image'
                                      />
                                    </a>
                                  </Link>
                                ) : (
                                  <img
                                    className='img-responsive img-responsive--cover'
                                    src={`${API_URL}${elem.image.url}`}
                                    style={
                                      news
                                        ? { maxHeight: '70%' }
                                        : { maxHeight: '100%' }
                                    }
                                    alt='slider image'
                                  />
                                )}
                              </div>
                              <p className='col-lg-6 news-slide-text d-block d-md-none stage__text stage__text--mobile text text--regular'>
                                {elem.intro.text}
                              </p>
                            </div>
                          );
                        } else {
                          return null;
                        }
                      })}
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
      ) : null}
    </>
  );
};

export default Slider;
