import Markdown from '../Markdown';

const PageDescription = ({ title, text, hidden, small }) => {
  return (
    <>
      {!hidden ? (
        <section
          className={
            small ? 'page-info' : 'page-info section-margin-content-sm'
          }
        >
          <div className='container'>
            <h2 className='page-description__title'>{title && title}</h2>
            <div className='row'>
              {text &&
                text.map((item) => (
                  <Markdown
                    key={item.id && item.id}
                    className='page-description__text col-sm-12 col-md-6'
                    source={item.text && item.text}
                  />
                ))}
            </div>
          </div>
        </section>
      ) : null}
    </>
  );
};

export default PageDescription;
