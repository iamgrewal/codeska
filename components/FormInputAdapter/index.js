const FormInputAdapter = ({ input, meta, className, placeholder }) => {
  return (
    <>
      <input
        {...input}
        type={input.type}
        name={input.name}
        className={
          meta.error && meta.touched
            ? `${className} error`
            : meta.valid
            ? `${className} agreed`
            : `${className}`
        }
        placeholder={placeholder}
      />
    </>
  );
};

export default FormInputAdapter;
