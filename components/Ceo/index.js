import Link from 'next/link';
const { API_URL } = process.env;
import Markdown from '../Markdown';
import InfoBar from '../Infobar';

const Ceo = ({ ceo, company }) => {
  return (
    <>
      {!ceo?.hidden ? (
        <section className='ceo'>
          <div className='container'>
            {ceo?.infobar?.theme && (
              <InfoBar
                date=''
                title={ceo.infobar.theme}
                commonClassName=''
                hrefLink='/expertise'
              />
            )}
            <div className='row flex-column-reverse flex-md-row'>
              <div className='col-md-6 col-lg-6 col-xl-7 seo-content-wrap'>
                <Markdown
                  className='ceo__slogan d-none d-md-block'
                  source={ceo?.slogan}
                />
                <div className='ceo__person'>
                  <Markdown
                    className='ceo__caption caption caption--h3'
                    source={ceo?.title}
                  />
                  {ceo?.paragraph?.map((item) => (
                    <Markdown
                      className='ceo__text'
                      key={item?.id}
                      source={item?.text}
                    />
                  ))}
                  {company && ceo?.link_text ? (
                    <Link href='/expertise'>
                      <a className='ceo__link hero-link'>{ceo?.link_text}</a>
                    </Link>
                  ) : null}
                </div>
              </div>

              <div className='col-5 col-sm-5 col-md-6 col-lg-6 col-xl-5 ceo__image-wrapper'>
                <img
                  className='img-responsive img-responsive--cover'
                  src={ceo?.image?.url && `${API_URL}${ceo.image.url}`}
                  alt='codeska ceo image'
                />
              </div>
              <div className='col-md-6 col-lg-6 col-xl-7 d-block d-md-none'>
                <Markdown className='ceo__slogan' source={ceo?.slogan} />
              </div>
            </div>
          </div>
        </section>
      ) : null}
    </>
  );
};

export default Ceo;
