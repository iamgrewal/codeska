import Link from 'next/link';
import Markdown from '../Markdown';
import InfoBar from '../Infobar';

const OurServices = ({ services }) => {
  return (
    <>
      {!services.hidden ? (
        <section className='services'>
          <div className='container'>
            {services.infobar.theme ? (
              <InfoBar
                date=''
                title={services.infobar.theme}
                commonClassName=''
                hrefLink='/services'
              />
            ) : null}
            <div className='row'>
              <div className='services__promo col-xl-6'>
                <Markdown
                  className='services__promo-text'
                  source={services.info ? services.info : null}
                />
                {services.link_text ? (
                  <Link href='/services'>
                    <a className='services__link hero-link'>
                      {services.link_text}
                    </a>
                  </Link>
                ) : null}
              </div>
            </div>
            <div className='row'>
              {services.content
                ? services.content.map((item) => (
                    <div key={item._id} className='col-lg-4 services__col'>
                      <div className='services__item'>
                        <Markdown
                          className='services__item-caption caption caption--h3 caption--700'
                          source={item.title ? item.title : null}
                        />
                        <Markdown
                          className='services__item-text'
                          source={item.text ? item.text : null}
                        />
                      </div>
                    </div>
                  ))
                : null}
            </div>
          </div>
        </section>
      ) : null}
    </>
  );
};

export default OurServices;
