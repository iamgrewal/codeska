import Link from 'next/link';
const { API_URL } = process.env;
import Banner from '../Banner';
import InfoBar from '../Infobar';

const NewsPreview = ({ id, image, title, subTitle, infobar, banner }) => {
  return (
    <>
      <article className='article-prev'>
        <div className='container'>
          <InfoBar
            date={infobar.date}
            title={infobar.theme}
            commonClassName=''
          />
          <div className='row'>
            <div className='col-md-6 col-lg-6 article-prev__image-wrapper image-wrapper'>
              <img
                className='img-responsive img-responsive--cover'
                src={`${API_URL}${image.url}`}
                alt='article preview'
              />
            </div>
            <div className='col-md-6 col-lg-6'>
              <h3 className='article-prev__caption caption caption--h2 caption--800'>
                {title}
              </h3>
              <p className='article-prev__description'>{subTitle}</p>
              <Link
                href={{
                  pathname: `/news/${title}`,
                  query: { id: id },
                }}
              >
                <a className='article-prev__link hero-link'>read more</a>
              </Link>
            </div>
          </div>
        </div>
      </article>
      {/* <Banner key={banner.id} data={banner} /> */}
    </>
  );
};

export default NewsPreview;
