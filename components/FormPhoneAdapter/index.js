import PhoneInput from 'react-phone-input-2';
// import PhoneInput from "react-phone-number-input";

const FormPhoneAdapter = ({ input, meta, className, ...rest }) => {
  return (
    <PhoneInput
      value={input.value.value}
      onChange={(value) => {
        
        return input.onChange(value);
      }}
      containerClass={className}
      inputClass='nested-input'
      buttonClass='nested-dropdown'
      inputExtraProps={{
        name: 'phone',
        required: true,
        autoFocus: false,
      }}
      placeholder='phone*'
      country='ua'
    />
  );
};

export default FormPhoneAdapter;
