import { useState } from 'react';
import Link from 'next/link';
import Popup from 'components/Popup';
const { API_URL } = process.env;

const Footer = ({ data, menu }) => {
  const contacts = data[0] !== undefined ? data[0].contacts : null;
  const contact_us = data[0] !== undefined ? data[0].contact_us : null;
  const logos = data[0] !== undefined ? data[0].logo_link : null;
  const copyright = data[0] !== undefined ? data[0].copyright : null;
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const openModal = () => {
    setModalIsOpen(true);
  };
  return (
    <footer className='footer'>
      <div className='container'>
        <div className='row footer__row'>
          <div className='footer_menu col-md-9 col-lg-12 col-xl-2 d-flex d-xl-none'>
            <ul className='contacts-list contacts-list--navigation col-12 p-0'>
              {menu.map((item) => {
                if (item.name !== 'home') {
                  return (
                    <li
                      className='contacts-group__title col-6 contacts-col'
                      key={item.id}
                    >
                      <Link href={`${item.path}`}>
                        <a className='contacts-list__link'>{item.name}</a>
                      </Link>
                    </li>
                  );
                }
              })}
            </ul>
          </div>
          <div className='contacts-group contacts-people col-12 col-md-9 col-lg-12 col-xl-4'>
            {contacts
              ? contacts.map((item) => {
                  if (!item.hidden) {
                    return (
                      <div
                        className='footer-cont col-6 col-md-6 col-lg-6 col-xl-12'
                        key={item.id}
                      >
                        <div className='footer__img'>
                          {item.image && (
                            <img
                              className='img-responsive img-responsive--cover'
                              src={`${API_URL}${item.image.url}`}
                              alt='image'
                            />
                          )}
                        </div>

                        <ul className='contacts-list'>
                          <li className='contacts-list__title'>
                            {item.title_one && item.title_one}
                          </li>
                          <li className='contacts-list__title contacts-list__title--sm'>
                            {item.title_two && item.title_two}
                          </li>
                          <hr></hr>
                          <li className='contacts-list__item'>
                            <p className='contacts-list__link no-hover'>
                              {item.phone_one && item.phone_one}
                            </p>
                          </li>
                          <li className='contacts-list__item'>
                            <p className='contacts-list__link no-hover'>
                              {item.phone_two && item.phone_two}
                            </p>
                          </li>
                          <li className='contacts-list__item'>
                            <p className='contacts-list__link no-hover'>
                              {item.email && item.email}
                            </p>
                          </li>
                        </ul>
                      </div>
                    );
                  } else {
                    return null;
                  }
                })
              : null}
          </div>
          <div className='footer_menu col col-lg-12 col-xl-1 d-none d-xl-flex'>
            <div>
              <ul className='contacts-list contacts-list--navigation'>
                {menu.map((item) => {
                  if (item.name !== 'home') {
                    return (
                      <li className='contacts-group__title' key={item.id}>
                        <Link href={`${item.path}`}>
                          <a className='contacts-list__link '>{item.name}</a>
                        </Link>
                      </li>
                    );
                  }
                })}
              </ul>
            </div>
          </div>

          <div className='footer__feedback col-sm-12 col-lg-8 col-xl-4'>
            {!contact_us.hidden && (
              <>
                <span className='footer__caption caption caption--h2 caption--800'>
                  {contact_us.title && contact_us.title}
                </span>
                <p className='footer__text'>
                  {contact_us.text && contact_us.text}
                </p>
                <button
                  className='footer__btn btn btn--contact'
                  onClick={openModal}
                  type='button'
                  id='btn-contact'
                >
                  <span> {contact_us.button_text}</span>
                </button>
              </>
            )}

            <div className='contacts-group contacts-group--logos'>
              {logos.map((item) => {
                if (!item.hidden) {
                  return (
                    <a
                      target='_blank'
                      rel='noreferrer'
                      href={item.link}
                      key={item.id}
                    >
                      {item.logo && (
                        <img
                          className='contacts-group__logo'
                          src={`${API_URL}${item.logo.url}`}
                          alt='logo'
                        />
                      )}
                    </a>
                  );
                } else {
                  return null;
                }
              })}
            </div>
          </div>
        </div>
        <span className='footer__copyright'>{copyright && copyright}</span>
      </div>
      <Popup isOpen={modalIsOpen} setModalState={setModalIsOpen} />
    </footer>
  );
};

export default Footer;
