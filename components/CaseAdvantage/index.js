import { useEffect, useRef } from 'react';
import Glide from '@glidejs/glide';
const { API_URL } = process.env;

const CaseAdvantage = ({ advantages_title, advantages_item }) => {
  const advantageRef = useRef();

  useEffect(() => {
    const slider = new Glide(advantageRef.current, {
      type: `carousel`,
      startAt: 0,
      gap: 30,
      animationDuration: 600,
      focusAt: `center`,
      perView: 1,
    });
    slider.mount();
  }, [advantageRef]);
  return (
    <section className='row case__advantages section-margin'>
      <div className='container'>
        <h2 className='case__caption caption caption--h2 caption--800'>
          {advantages_title?.title}
        </h2>
        {!advantages_item.hidden && (
          <div className='row advantages d-none d-lg-flex'>
            {advantages_item?.length !== 0
              ? advantages_item.map((elem) => (
                  <div className='col-6 ' key={elem.id}>
                    <div className='advantages__wrapper'>
                      <p className='advantages__benefit'>{elem?.benefits}</p>
                      {elem?.icon?.url && (
                        <img
                          className='mb-20'
                          src={`${API_URL}${elem.icon.url}`}
                          alt='sidebar-image'
                        />
                      )}
                      <p className='case__subcaption advantages__subcaption'>
                        {elem?.icon_subtext}
                      </p>

                      {elem?.text ? (
                        <p className='advantages__text'>{elem?.text}</p>
                      ) : null}
                      <ul className='list'>
                        {elem?.item?.length !== 0
                          ? elem?.item.map((point, i) => (
                              <p className='list__item' key={i}>
                                {' '}
                                {point?.item}{' '}
                              </p>
                            ))
                          : null}
                      </ul>
                    </div>
                  </div>
                ))
              : null}
          </div>
        )}

        {/* MOBILE */}
        {!advantages_item.hidden && (
          <div className='d-block d-lg-none'>
            <div
              className='glide glide--ltr glide--slider glide--swipeable'
              ref={advantageRef}
            >
              <div
                className='supply-schema__track glide__track'
                data-glide-el='track'
              >
                <div className='glide__slides'>
                  {advantages_item?.length !== 0
                    ? advantages_item?.map((elem, i) => (
                        <div className='glide__slide' key={i}>
                          <div className='advantages__wrapper'>
                            <p className='advantages__benefit'>
                              {elem?.benefits}
                            </p>
                            {elem?.icon?.url && (
                              <img
                                className='mb-20'
                                src={`${API_URL}${elem.icon.url}`}
                                alt='sidebar-image'
                              />
                            )}
                            <p className='case__subcaption advantages__subcaption'>
                              {elem?.icon_subtext}
                            </p>
                            {elem?.text ? (
                              <p className='advantages__text'>{elem?.text}</p>
                            ) : null}

                            <ul className='list'>
                              {elem?.item.length !== 0
                                ? elem?.item.map((point, i) => (
                                    <p className='list__item' key={i}>
                                      {' '}
                                      {point?.item}{' '}
                                    </p>
                                  ))
                                : null}
                            </ul>
                          </div>
                        </div>
                      ))
                    : null}
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </section>
  );
};

export default CaseAdvantage;
