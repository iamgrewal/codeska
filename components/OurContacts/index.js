const { API_URL } = process.env;
import Infobar from '../Infobar';

const OurContacts = ({ data }) => {
  return (
    <>
      {!data.hidden ? (
        <div className='container our-contact section-margin'>
          {data.infobar.theme && (
            <Infobar date='' title={data.infobar.theme} commonClassName='' />
          )}

          {data.contact.length &&
            data.contact.map((item) => (
              <div className='row our-contact__row' key={item.id}>
                <div className='col-12 col-md-6 col-xl-7 our-contact__image-wrapper'>
                  <img
                    className='img-responsive img-responsive--cover'
                    src={item.image.url && `${API_URL}${item.image.url}`}
                  />
                </div>
                <ul className='our-contact__list col-12 col-md-6 col-xl-4'>
                  <div className='our-contact__title-wrapper'>
                    <li className='our-contact__title'>
                      {item.title_one && item.title_one}
                    </li>
                    <li className='our-contact__item our-contact__item--person'>
                      <p className='our-contact__link'>
                        {item.title_two && item.title_two}
                      </p>
                    </li>
                  </div>

                  <li className='our-contact__item'>
                    <p className='our-contact__link'>
                      {item.phone_one && item.phone_one}
                    </p>
                  </li>
                  <li className='our-contact__item'>
                    <p className='our-contact__link'>
                      {item.phone_two && item.phone_two}
                    </p>
                  </li>

                  <li className='our-contact__item'>
                    <p className='our-contact__link'>
                      {item.email && item.email}
                    </p>
                  </li>
                  {item.who && (
                    <div className='our-contact__social d-flex'>
                      <a
                        target='_blank'
                        href={
                          item.who === 'lander'
                            ? 'https://www.facebook.com/andrew.lander.39'
                            : 'https://www.facebook.com/Codeska/'
                        }
                      >
                        <img
                          className='contacts-group__logo contacts-group__logo--contact'
                          src={require('images/icons/facebook.svg')}
                          alt='logo'
                        />
                      </a>
                      <a
                        target='_blank'
                        href={
                          item.who === 'lander'
                            ? 'https://www.linkedin.com/in/andrew-lander-3848244a/'
                            : 'https://www.linkedin.com/company/codeska/'
                        }
                      >
                        <img
                          className='contacts-group__logo contacts-group__logo--contact'
                          src={require('images/icons/linkedin.svg')}
                          alt='logo'
                        />
                      </a>
                    </div>
                  )}
                </ul>
              </div>
            ))}
        </div>
      ) : null}
    </>
  );
};

export default OurContacts;
