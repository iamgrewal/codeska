import { useEffect, useState, useRef } from 'react';
import Glide from '@glidejs/glide';
import sliderConfiguration from '../../config/sliderConfiguration';
import InfoBar from '../Infobar';

const { API_URL } = process.env;

const Partners = ({ partners }) => {
  const { hidden, infobar, partner_block } = partners;
  const [activeArr, setActiveArr] = useState(false);
  const [clickedArr, setClickedArr] = useState(null);
  const changeActiveArr = (id) => {
    setClickedArr(id);
    setActiveArr(!activeArr);
  };

  const ref = useRef();
  useEffect(() => {
    const slider = new Glide(ref.current, sliderConfiguration);
    if (slider && ref.current) {
      slider.mount();
    }
  }, [ref]);
  return (
    <>
      {!hidden ? (
        <>
          <section className='partners d-none d-lg-block'>
            <div className='container'>
              {infobar && (
                <InfoBar date='' title={infobar.theme} commonClassName='' />
              )}
              <ul className='row partners__list'>
                {partner_block &&
                  partner_block.map((item) => (
                    <li
                      key={item._id}
                      className='partners__item col-sm-12 col-md-6 col-lg-4 col-xl-4 '
                    >
                      <div className='partners__item-image'>
                        {item.logo && (
                          <img
                            src={item.logo.url && `${API_URL}${item.logo.url}`}
                            alt='partner'
                          />
                        )}
                      </div>
                      <span className='partners__item-title'>
                        {item.partner_name}
                      </span>
                      <p className='partners__item-about'>{item.text}</p>
                      <a
                        className='partners__item-link hero-link hero-link--sm'
                        target='_blank'
                        href={item.link}
                      >
                        {item.link}
                      </a>
                    </li>
                  ))}
              </ul>
            </div>
          </section>
          <section className='partners d-sm-block d-lg-none'>
            <div className='container'>
              <InfoBar date='' title='partners' commonClassName='' />
              <div
                className='glide glide--ltr glide--slider glide--swipeable'
                ref={ref}
              >
                <div className='partners__bullets-wrapper'>
                  <div className='glide__arrows' data-glide-el='controls'>
                    <button
                      className='glide__arrow glide__arrow--left'
                      id={0}
                      data-glide-dir='<'
                      onClick={() => changeActiveArr(0)}
                    >
                      <img
                        className='slider__btn slider__btn--prev'
                        src={
                          clickedArr === 0
                            ? require('images/icons/slider-arr-active.svg')
                            : require('images/icons/slider-arr.svg')
                        }
                      />
                    </button>
                  </div>
                  <div
                    className=' glide__bullets'
                    data-glide-el='controls[nav]'
                  >
                    {partner_block.length !== 0
                      ? partner_block.map((item, index) => (
                          <button
                            className='partners__slider-bullet glide__bullet'
                            data-glide-dir={`=${index}`}
                            key={index}
                          ></button>
                        ))
                      : null}
                  </div>

                  <div className='glide__arrows' data-glide-el='controls'>
                    <button
                      className='glide__arrow glide__arrow--right'
                      id={1}
                      data-glide-dir='>'
                      onClick={() => changeActiveArr(1)}
                    >
                      <img
                        className='slider__btn slider__btn--next'
                        src={
                          clickedArr === 1
                            ? require('images/icons/slider-arr-active.svg')
                            : require('images/icons/slider-arr.svg')
                        }
                      />
                    </button>
                  </div>
                </div>
                {/* slide track */}
                <div
                  className='supply-schema__track glide__track'
                  data-glide-el='track'
                >
                  <div className='glide__slides'>
                    {partner_block &&
                      partner_block.map((item, index) => (
                        <div className='glide__slide' key={index}>
                          <li key={item._id} className='partners__item col-12 '>
                            <div className='partners__item-image'>
                              {item.logo && (
                                <img
                                  src={`${API_URL}${item.logo.url}`}
                                  alt='partner'
                                />
                              )}
                            </div>
                            <span className='partners__item-title'>
                              {item.partner_name}
                            </span>
                            <p className='partners__item-about'>{item.text}</p>
                            <a
                              className='partners__item-link'
                              target='_blank'
                              href={item.link}
                            >
                              {item.link}
                            </a>
                          </li>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
      ) : null}
    </>
  );
};

export default Partners;
