import Link from "next/link";

const InfoBar = ({ date, title, commonClassName, hrefLink }) => {
  return (
    <div className={`info-bar ${commonClassName}`}>
      {date ? (
        <>
          <span className="info-bar__date">{date}</span>{" "}
          <h2 className="info-bar__title">{title}</h2>
        </>
      ) : (
        <Link href={hrefLink ? hrefLink : " "}>
          <a>
            <h2 className="info-bar__title">{title}</h2>
          </a>
        </Link>
      )}
    </div>
  );
};

export default InfoBar;
