const Icon = ({ name, mod }) => {
  if (mod) {
    return (
      <svg className={"icon-" + name + " " + mod}>
        <use xlinkHref="/images/sprite/sprite.svg#" />
      </svg>
    );
  } else {
    return (
      <svg className={"icon-" + name}>
        <use xlinkHref="/images/sprite/sprite.svg#" />
      </svg>
    );
  }
};

export default Icon;
