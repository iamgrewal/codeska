const EmbedFeed = ({ data }) => {
  return (
    <>
      {!data.hidden ? (
        <section className='section-margin'>
          <div className='container'>
            <div className='row'>
              {data.feed.length !== 0 &&
                data.feed.map((item) => (
                  <div className='col-xl-6' key={item.id}>
                    <iframe
                      height={item.height && item.height}
                      src={item.link && item.link}
                      style={{
                        border: 'none',
                        overflow: 'hidden',
                        width: '100%',
                      }}
                      scrolling='no'
                      frameBorder='0'
                      allowtransparency='true'
                      allow='encrypted-media'
                    ></iframe>
                  </div>
                ))}
            </div>
          </div>
        </section>
      ) : null}
    </>
  );
};

export default EmbedFeed;
