import { useCallback } from 'react';

const CheckBox = ({ onChange, checked, ...rest }) => {
  const handleChange = useCallback(
    (e) => {
      if (typeof onChange === 'function') {
   
        onChange(e.target.checked);
      }
    },
    [onChange]
  );

  return (
    <label className='checkbox-wrap'>
      <input {...rest} onChange={handleChange} type='checkbox' />
      <span
        className='mark'
        style={
          checked
            ? { border: '2px solid #23c1c1' }
            : { border: '2px solid #ccc' }
        }
      />
    </label>
  );
};

export default CheckBox;
