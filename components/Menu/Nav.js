import Link from 'next/link';

const HeaderNav = ({ menu, path }) => {
  return (
    <>
      <nav
        className='main-nav'
        style={
          path === '/404'
            ? { display: 'flex', alignItems: 'center' }
            : { display: 'block' }
        }
      >
        {path === '/404' && (
          <Link href='/'>
            <a className='site-menu__logo'>
              <img
                src={require('images/icons/codeska-logo-white.svg')}
                alt='logo'
              />
            </a>
          </Link>
        )}
        <ul className='main-nav__list header__nav'>
          {menu.map((item) => {
            if (item.name !== 'home' && item.name !== 'social responsibility') {
              return (
                <li className='main-nav__item' key={item.id}>
                  <Link href={`${item.path}`}>
                    <a
                      className='main-nav__link'
                      style={
                        path === '/404'
                          ? { color: '#fff' }
                          : { color: 'inherit' }
                      }
                    >
                      {item.name}
                    </a>
                  </Link>
                </li>
              );
            }
          })}
        </ul>
      </nav>
    </>
  );
};

export default HeaderNav;
