import { useState, useCallback } from 'react';
import Link from 'next/link';
import { Container } from 'react-bootstrap';
import HomePreview from './menuPreview/HomePreview';
const { API_URL } = process.env;

const Menu = ({ menu, path }) => {
  const [menuActive, toggleMenuActive] = useState(false);

  const closeMenu = () => {
    toggleMenuActive(false);
  };
  const toggleMenu = () => {
    toggleMenuActive(!menuActive);
  };

  const activeState = useCallback(() => {
    if (menuActive) return `is-active`;
    else return ``;
  }, [menuActive]);

  const item = menu.find((item) => item.navigation_logo);
  const svg = item.navigation_logo;
  const info = item.info;

  return (
    <>
      <div
        className='site-menu'
        style={path === '/404' ? { display: 'none' } : { display: 'block' }}
      >
        <div className='site-menu__panel '>
          <div className='site-menu__wrapper'>
            <Link href='/'>
              <a className='site-menu__logo'>
                <img src={`${API_URL}${svg.logo.url}`} alt='logo' />
              </a>
            </Link>
            <button
              className={`sandwich site-menu__open-btn  ${activeState()}`}
              onClick={() => toggleMenu()}
            >
              <span className='sandwich__bars'></span>
            </button>
            <span className='site-menu__panel-slogan'>{svg.slogan}</span>
          </div>
        </div>
        <nav className={`site-menu__nav menu ${activeState()}`}>
          <Container className='container--menu'>
            <div className='menu__nav'>
              <button
                className='menu__nav-btn menu-close-btn'
                type='button'
                onClick={() => toggleMenu()}
              >
                <img
                  className='icon-arrow menu-close-btn__icon'
                  src={`${API_URL}${svg.arrow_back.url}`}
                  alt='logo'
                />
                <span className='visually-hidden'>Close menu</span>
              </button>
              <ul className='menu__list'>
                {menu.map((item) => {
                  if (item.name !== 'social responsibility') {
                    return (
                      <li
                        className='menu__item'
                        key={item.id}
                        onClick={closeMenu}
                      >
                        <Link href={`${item.path}`}>
                          <a className='menu__link'>{item.name}</a>
                        </Link>
                      </li>
                    );
                  }
                })}
              </ul>
            </div>
            <div className='menu__info-nav'>
              <ul className='menu__previews menu-preview'>
                <HomePreview data={info} />
              </ul>
            </div>
          </Container>
        </nav>
      </div>
    </>
  );
};

export default Menu;
