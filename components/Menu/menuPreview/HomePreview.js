const { API_URL } = process.env;

const HomePreview = ({ data }) => {
  return (
    <li className='menu-preview__item menu-preview__item--main'>
      <div className='contacts-group contacts-group--menu col-lg-12 col-xl-12'>
        {data.contacts && data.contacts.length !== 0
          ? data.contacts.map((item) => {
              if (!item.hidden) {
                return (
                  <div className='footer-cont footer-cont--menu' key={item.id}>
                    <div className='footer__img footer__img--menu'>
                      <img
                        className='img-responsive img-responsive--cover'
                        src={`${API_URL}${item.image.url}`}
                        alt='image'
                      />
                    </div>
                    <ul className='contacts-list'>
                      <li className='contacts-list__title contacts-list__title--menu'>
                        {item.title_one ? item.title_one : null}
                      </li>
                      <li className='contacts-list__title contacts-list__title--sm contacts-list__title--menu'>
                        {item.title_two ? item.title_two : null}
                      </li>
                      <hr></hr>
                      <li className='contacts-list__item '>
                        <p className='contacts-list__link contacts-list__link--menu no-hover'>
                          {item.phone_one ? item.phone_one : null}
                        </p>
                      </li>
                      <li className='contacts-list__item '>
                        <p className='contacts-list__link contacts-list__link--menu no-hover'>
                          {item.phone_two ? item.phone_two : null}
                        </p>
                      </li>
                      <li className='contacts-list__item '>
                        <p className='contacts-list__link contacts-list__link--menu no-hover'>
                          {item.email ? item.email : null}
                        </p>
                      </li>
                    </ul>
                  </div>
                );
              } else {
                return null;
              }
            })
          : null}
        <div className='contacts-group contacts-group--logos contacts-group--logos-menu'>
          {data.logo_link && data.logo_link.length !== 0
            ? data.logo_link.map((item) => {
                if (!item.hidden) {
                  return (
                    <a target='_blank' href={item.link} key={item.id}>
                      <img
                        className='contacts-group__logo'
                        src={`${API_URL}${item.logo.url}`}
                        alt='logo'
                      />
                    </a>
                  );
                } else {
                  return null;
                }
              })
            : null}
        </div>
      </div>
    </li>
  );
};

export default HomePreview;
