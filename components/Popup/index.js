import { useState, useEffect } from 'react';
import Modal from 'react-modal';
import ApplyForm from 'components/Form';

Modal.setAppElement('#__next');

const Popup = ({ isOpen, setModalState, careerForm }) => {
  const [modalIsOpen, setModalIsOpen] = useState(false);

  useEffect(() => {
    if (isOpen === true) {
      setModalIsOpen(true);
    }
  }, [isOpen]);

  const closeModal = () => {
    setModalIsOpen(false);
    setModalState(false);
  };

  return (
    <>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        shouldCloseOnOverlayClick
        className={
          modalIsOpen ? 'contact-popup popup-is-active' : 'contact-popup fade'
        }
        overlayClassName='overlay'
      >
        <button
          className='contact-popup__close-btn'
          type='button'
          aria-label='close contact form'
          onClick={closeModal}
        >
          <img
            className='contact-popup__close-icon'
            src={require('images/icons/close.svg')}
            alt='close'
          />
        </button>

        <ApplyForm closeModal={closeModal} careerForm={careerForm} />
      </Modal>
    </>
  );
};

export default Popup;
